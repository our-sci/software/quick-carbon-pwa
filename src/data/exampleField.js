const exampleField = {
	id: '1bfec9598c0838d4c2b51c27c6064088',
	type: 'Feature',
	properties: {
		handle: 'handle',
		organization: 1,
		name: 'field 1',
		depths: ['0_TO_10_CM'],
		hasAdditionalQuestions: false,
		email: 'farmerbob@farm.com',
		createdAt: 1568683545888,
	},
	geometry: {
		// coordinates: [
		// 	[
		// 		[-121.26298346569213, 43.341511169604644],
		// 		[-125.05348891752874, 43.16277300641863],
		// 		[-124.46276079516468, 41.34640252262025],
		// 		[-121.55834752687429, 41.67723256139456],
		// 		[-121.26298346569213, 43.341511169604644]
		// 	]
		// ],
		coordinates: [
			[
				[-122.47511454170204, 37.80847201259607],
				[-122.48400292595022, 37.79062126639059],
				[-122.45733777320572, 37.79193827209271],
				[-122.46215231467343, 37.80466811692327],
				[-122.47511454170204, 37.80847201259607],
			],
		],
		type: 'Polygon',
	},
};

const fieldFeatureCollection = {
	type: 'FeatureCollection',
	features: [exampleField],
};

export default exampleField;
export {
	fieldFeatureCollection,
};
