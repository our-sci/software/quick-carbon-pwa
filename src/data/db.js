import locationCollections from 'data/exampleLocationCollections';

const organizations = [
	// {
	// 	id: null,
	// 	name: 'Organization',
	// 	disabled: true,
	// },
	{
		id: 1,
		name: 'Our Sci',
		handle: 'our-sci',
	},
	{
		id: 2,
		name: 'Snapp Lab',
		handle: 'snapp-lab',
	},
	{
		id: 3,
		name: 'Real Food Campaign',
		handle: 'real-food-campaign',
	},
];

// const fields = [
// 	{
// 		id: 1,
// 		name: 'Field 1',
// 		samplingEvents: [1, 3],
// 		geometry: {
// 			coordinates: [
// 				[
// 					[-121.21853916959321, 37.765037215002636],
// 					[-120.73850845825076, 36.75901481152573],
// 					[-119.36242041906883, 37.95453275922172],
// 					[-120.67450436340502, 38.206434872085254],
// 					[-121.21853916959321, 37.765037215002636],
// 				],
// 			],
// 			type: 'Polygon',
// 		},
// 	},
// 	{
// 		id: 2,
// 		name: 'Field 2',
// 		samplingEvents: [2, 4],
// 		geometry: {
// 			coordinates: [
// 				[
// 					[-122.7562035156246, 40.062320141059075],
// 					[-123.06382070312497, 39.50511291289541],
// 					[-122.40464101562465, 38.85790225917671],
// 					[-121.65757070312466, 39.369353448076964],
// 					[-122.7562035156246, 40.062320141059075],
// 				],
// 			],
// 			type: 'Polygon',
// 		},
// 	},
// ];
const fields = [
	{
		id: 1,
		type: 'Feature',
		geometry: {
			coordinates: [
				[
					[-121.21853916959321, 37.765037215002636],
					[-120.73850845825076, 36.75901481152573],
					[-119.36242041906883, 37.95453275922172],
					[-120.67450436340502, 38.206434872085254],
					[-121.21853916959321, 37.765037215002636],
				],
			],
			type: 'Polygon',
		},
		properties: {
			name: 'Field 1',
			samplingEvents: [1, 3],
			handle: 'field-1',
			organization: 1,
			depths: ['0_TO_10_CM'],
			hasAdditionalQuestions: false,
			email: 'bob@bob.com',
			createdAt: 1568859006322,
			surveyLocations: 1,
		},
	},
	{
		id: 2,
		type: 'Feature',
		geometry: {
			coordinates: [
				[
					[-122.7562035156246, 40.062320141059075],
					[-123.06382070312497, 39.50511291289541],
					[-122.40464101562465, 38.85790225917671],
					[-121.65757070312466, 39.369353448076964],
					[-122.7562035156246, 40.062320141059075],
				],
			],
			type: 'Polygon',
		},
		properties: {
			name: 'Field 2',
			samplingEvents: [2, 4],
			handle: 'field-2',
			organization: 2,
			depths: ['10_TO_20_CM', '20_TO_30_CM'],
			hasAdditionalQuestions: true,
			email: 'alice@sender.com',
			createdAt: 1568859006322,
			surveyLocations: 2,
		},
	},
	{
		id: '9f9e29285ccfbe0ea8be1037fc1c20bb',
		type: 'Feature',
		properties: {
			handle: 'b-woods-sort-of',
			organization: 1,
			name: 'B Woods Sort Of',
			depths: ['0_TO_10_CM', '20_TO_30_CM', '10_TO_20_CM'],
			hasAdditionalQuestions: true,
			email: 'asdf@asdf.com',
			createdAt: 1569102160508,
			surveyLocations: 3,
		},
		geometry: {
			coordinates: [
				[
					[-84.51026189492357, 39.1378136227479],
					[-84.51136949160868, 39.13952891835336],
					[-84.51312082543197, 39.139555363217596],
					[-84.51455279978865, 39.13876249235014],
					[-84.51421996858164, 39.137778953563014],
					[-84.51026189492357, 39.1378136227479],
				],
			],
			type: 'Polygon',
		},
	},
];


const surveyLocations = [
	{
		id: 1,
		// ...JSON.parse("{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.65746161537712,38.13002492488401]},\"id\":0},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-121.05829801379134,37.81629606577075]},\"id\":1},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.61703676782135,37.7357241350753]},\"id\":2},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-119.91390875993886,37.57821840591237]},\"id\":3},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-119.64601421491118,37.86621546028642]},\"id\":4},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.412752989259,37.638605926703015]},\"id\":5},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.75271485380483,37.62906918844378]},\"id\":6},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.89854342482975,37.54958728925132]},\"id\":7},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.66896472487349,37.629812277755526]},\"id\":8},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.19235197205546,38.06621784974456]},\"id\":9},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.46401636216451,37.29281687369119]},\"id\":10},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-121.01837627663734,37.857639258716546]},\"id\":11},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.3054892460227,38.05045297973783]},\"id\":12},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.76137517412737,37.670148763116856]},\"id\":13},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.71948429170561,38.05901871268068]},\"id\":14},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-119.65800420421292,37.98016802520298]},\"id\":15},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.4219125924259,38.07498500937788]},\"id\":16},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.82704826409358,37.78700860206555]},\"id\":17},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.89542720528142,37.513798284422485]},\"id\":18},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-119.937438958976,37.98783195044137]},\"id\":19},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.44000821851898,37.59186240003619]},\"id\":20},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.44551304712536,37.38836521219901]},\"id\":21},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.83638452069232,37.98892570026524]},\"id\":22},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.30980509415168,37.446505704952095]},\"id\":23},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-120.37253014478962,37.992725348423285]},\"id\":24}]}"),
		...locationCollections[0],
	},
	{
		id: 2,
		// ...JSON.parse("{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.28350067666945,39.46212723452533]},\"id\":0},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.26171655600642,39.03062408613256]},\"id\":1},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.49768536790249,39.757814167244064]},\"id\":2},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.52177334334893,39.03163319285841]},\"id\":3},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-123.00848383132139,39.59634409047942]},\"id\":4},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.66811292006783,39.430786823817314]},\"id\":5},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.55729568457066,39.62888137976534]},\"id\":6},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.55175403849829,39.83643754333679]},\"id\":7},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.55328539175444,39.57647424788751]},\"id\":8},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.88558997288922,39.34370656660985]},\"id\":9},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.35122775859152,39.122819755056284]},\"id\":10},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.38197682091943,39.7086004701415]},\"id\":11},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.35548881771628,39.21634416068161]},\"id\":12},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.49671175082433,39.44134563048453]},\"id\":13},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.50592885641743,39.53023784128188]},\"id\":14},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.19205350451381,39.02007587334336]},\"id\":15},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.56874059123697,39.56809246434962]},\"id\":16},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-121.99888187881423,39.15748418861727]},\"id\":17},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.00111544673088,39.13837902783829]},\"id\":18},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.68604484095903,39.18141566256271]},\"id\":19},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.41015237432656,39.52065605535456]},\"id\":20},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.21350653359609,39.641355602131235]},\"id\":21},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.35276646644485,39.422143962337685]},\"id\":22},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.55949949798926,39.241366359087934]},\"id\":23},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.66067710048291,39.83286298036715]},\"id\":24}]}"),
		...locationCollections[1],
	},
	{
		id: 3,
		// ...JSON.parse("{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.28350067666945,39.46212723452533]},\"id\":0},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.26171655600642,39.03062408613256]},\"id\":1},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.49768536790249,39.757814167244064]},\"id\":2},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.52177334334893,39.03163319285841]},\"id\":3},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-123.00848383132139,39.59634409047942]},\"id\":4},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.66811292006783,39.430786823817314]},\"id\":5},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.55729568457066,39.62888137976534]},\"id\":6},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.55175403849829,39.83643754333679]},\"id\":7},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.55328539175444,39.57647424788751]},\"id\":8},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.88558997288922,39.34370656660985]},\"id\":9},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.35122775859152,39.122819755056284]},\"id\":10},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.38197682091943,39.7086004701415]},\"id\":11},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.35548881771628,39.21634416068161]},\"id\":12},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.49671175082433,39.44134563048453]},\"id\":13},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.50592885641743,39.53023784128188]},\"id\":14},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.19205350451381,39.02007587334336]},\"id\":15},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.56874059123697,39.56809246434962]},\"id\":16},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-121.99888187881423,39.15748418861727]},\"id\":17},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.00111544673088,39.13837902783829]},\"id\":18},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.68604484095903,39.18141566256271]},\"id\":19},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.41015237432656,39.52065605535456]},\"id\":20},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.21350653359609,39.641355602131235]},\"id\":21},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.35276646644485,39.422143962337685]},\"id\":22},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.55949949798926,39.241366359087934]},\"id\":23},{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Point\",\"coordinates\":[-122.66067710048291,39.83286298036715]},\"id\":24}]}"),
		...locationCollections[2],
	},
];

const samplingEvents = [
	{
		id: 1,
		samples: [1],
		fieldId: 0,
		modified_at: null,
		started_at: null,
		completed_at: null,
	},
	{
		id: 2,
		samples: [],
		fieldId: 1,
		modified_at: null,
		started_at: null,
		completed_at: null,
	},
	{
		id: 3,
		samples: [],
		fieldId: 2,
		modified_at: null,
		started_at: null,
		completed_at: null,
	},
];

// const samples = [
// 	{
// 		id: 1,
// 		soil_depth: 1, // reference to SoilDepth ID
// 		sample_id: 'asdf123',
// 		timestamp: null,
// 	},
// 	{
// 		id: 2,
// 		soil_depth: 2, // reference to SoilDepth ID
// 		sample_id: 'bsdf123',
// 		timestamp: null,
// 	},
// 	{
// 		id: 3,
// 		soil_depth: 1, // reference to SoilDepth ID
// 		sample_id: 'csdf123',
// 		timestamp: null,
// 	},
// ];

const samples = [
	{
		type: 'Feature',
		id: 'XYjkHsTHRGLlscZn',
		properties: {
			id: 'XYjkHsTHRGLlscZn',
			surveyId: 'XYjllsTHRGLlscZq',
			locationId: 'XYbSjY5bi0QOMR1w',
			sampleId: 'b34f69',
			timestamp: '2019-09-23T15:23:13.824Z',
			depthId: '0_TO_10_CM',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.5135040109584, 39.13937683755256],
		},
	},
	{
		type: 'Feature',
		id: 'XYjkscTHRGLlscZo',
		properties: {
			id: 'XYjkscTHRGLlscZo',
			surveyId: 'XYjllsTHRGLlscZq',
			locationId: 'XYbSjY5bi0QOMR1w',
			sampleId: 'b34f70',
			timestamp: '2019-09-23T15:23:13.824Z',
			depthId: '10_TO_20_CM',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.5135040109584, 39.13937683755256],
		},
	},
	{
		type: 'Feature',
		id: 'XYjkssTHRGLlscZp',
		properties: {
			id: 'XYjkssTHRGLlscZp',
			surveyId: 'XYjllsTHRGLlscZq',
			locationId: 'XYbSjY5bi0QOMR1w',
			sampleId: 'b34f71',
			timestamp: '2019-09-23T15:23:13.824Z',
			depthId: '20_TO_30_CM',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.5135040109584, 39.13937683755256],
		},
	},
	{
		type: 'Feature',
		id: 'XYjlrsTHRGLlscZr',
		properties: {
			id: 'XYjlrsTHRGLlscZr',
			surveyId: 'XYjllsTHRGLlscZq',
			locationId: 'XYbSjY5bi0QOMR1x',
			sampleId: 'b34f72',
			timestamp: '2019-09-23T15:23:13.824Z',
			depthId: '0_TO_10_CM',
		},
		geometry: {
			type: 'Point',
			coordinates: [-84.5135040109584, 39.13937683755256],
		},
	},

];

const soilDepths = [
	{
		id: 1,
		handle: '0_TO_10_CM',
		name: '0 - 10 cm',
		min_depth: 0,
		max_depth: 10,
		units: 'CM',
	},
	{
		id: 2,
		handle: '10_TO_20_CM',
		name: '10 - 20 cm',
		min_depth: 10,
		max_depth: 20,
		units: 'CM',
	},
	{
		id: 3,
		handle: '20_TO_30_CM',
		name: '20 - 30 cm',
		min_depth: 20,
		max_depth: 30,
		units: 'CM',
	},
];

export {
	organizations,
	fields,
	samplingEvents,
	samples,
	soilDepths,
	surveyLocations,
};
