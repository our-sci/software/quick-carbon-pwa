const depths = [
	{
		id: 1,
		handle: '0_TO_10_CM',
		name: '0 - 10 cm',
		min_depth: 0,
		max_depth: 10,
		units: 'CM',
	},
	{
		id: 3,
		handle: '20_TO_30_CM',
		name: '20 - 30 cm',
		min_depth: 20,
		max_depth: 30,
		units: 'CM',
	},
	{
		id: 2,
		handle: '10_TO_20_CM',
		name: '10 - 20 cm',
		min_depth: 10,
		max_depth: 20,
		units: 'CM',
	},
];

const location = {
	type: 'Feature',
	id: 'XYbSjY5bi0QOMR1+',
	properties: {
		label: '14',
		type: 'NEAREST_LOCATION_MARKER',
		id: 'XYbSjY5bi0QOMR1+',
		featureIndex: 14,
		distanceToPoint: 0.05589003852115167,
	},
	geometry: {
		type: 'Point',
		coordinates: [-84.51180641060412, 39.13926236180337],
	},
};

const position = {
	coords: {
		accuracy: 1377,
		altitude: null,
		altitudeAccuracy: null,
		heading: null,
		latitude: 39.1397607,
		longitude: -84.5117219,
		speed: null,
	},
	timestamp: 1569254893063,
	type: 'geolocate.geolocate',
	target: null,
};

const surveyId = 'XYjt5nH+DnUqu6Ht';

export default {
	depths,
	location,
	position,
	surveyId,
};

export {
	depths,
	location,
	position,
	surveyId,
};
