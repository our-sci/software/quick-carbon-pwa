import React, { useState, useRef } from 'react';
// import AppContext from 'contexts/AppContext';
import MapGL, { GeolocateControl } from 'react-map-gl';
import DeckGL, { GeoJsonLayer } from 'deck.gl';
import Geocoder from 'react-map-gl-geocoder';
// import MapGLDraw, { EditorModes } from 'react-map-gl-draw';
import 'mapbox-gl/dist/mapbox-gl.css';
import 'react-map-gl-geocoder/dist/mapbox-gl-geocoder.css';
import styles from './MBMap.module.css';

const MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;

const initialMapState = {
	viewport: {
		width: 375,
		height: 500,
		latitude: 37.7577,
		longitude: -122.4376,
		zoom: 8,
	},
	searchResultLayer: null,
	features: [],
	selectedFeatureId: null,
	// selectedMode: EditorModes.DRAW_POLYGON,
};


export default function MBMap() {
	const [mapState, setMapState] = useState(initialMapState);
	const mapRef = useRef();
	const drawRef = useRef();

	function handleViewportChange(viewport) {
		setMapState({ ...mapState, viewport });
	}

	function handleGeocoderViewportChange(viewport) {
		// const geocoderDefaultOverrides = { transitionDuration: 1000 };
		handleViewportChange({
			...viewport,
			// ...geocoderDefaultOverrides,
		});
	}

	function handleOnResult(event) {
		const resultLayer = new GeoJsonLayer({
			id: 'search-result',
			data: event.result.geometry,
			getFillColor: [255, 0, 0, 128],
			getRadius: 1000,
			pointRadiusMinPixels: 10,
			pointRadiusMaxPixels: 10,
		});
		setMapState({
			...mapState,
			searchResultLayer: resultLayer,
		});
	}


	function handleDrawOnSelect({ selectedFeatureId }) {
		setMapState({ ...mapState, selectedFeatureId });
	}

	function handleDrawOnUpdate(features) {
		setMapState({ ...mapState, features });
	}

	function getDrawHandleStyle({ feature, featureState, vertexIndex, vertexState }) {
		return {
			fill: vertexState === `SELECTED` ? '#000' : '#aaa',
			stroke: vertexState === `SELECTED` ? '#000' : 'none'
		};
	}

	function getDrawFeatureStyle({ feature, featureState }) {
		return {
			stroke: featureState === `SELECTED` ? '#000' : 'none',
			fill: featureState === `SELECTED` ? '#080' : 'none',
			fillOpacity: 0.8,
		};
	}

	return (
		<MapGL
			ref={mapRef}
			mapboxApiAccessToken={MAPBOX_TOKEN}
			{...mapState.viewport}
			onViewportChange={handleViewportChange}
			mapStyle="mapbox://styles/mapbox/satellite-v9"
		>
			<GeolocateControl
				className={styles['geolocate-control']}
				positionOptions={{ enableHighAccuracy: true }}
				trackUserLocation={true}
			/>
			<Geocoder
				mapboxApiAccessToken={MAPBOX_TOKEN}
				mapRef={mapRef}
				onResult={handleOnResult}
				onViewportChange={handleGeocoderViewportChange}
				position="top-left"
				className="asdf"
			/>

			<DeckGL
				{...mapState.viewport }
				layers={[mapState.searchResultLayer]}
			/>
			{/* <MapGLDraw
				ref={drawRef}
				mode={mapState.selectedMode}
				features={mapState.features}
				selectedFeatureId={mapState.selectedFeatureId}
				onSelect={handleDrawOnSelect}
				onUpdate={handleDrawOnUpdate}
				getEditHandleStyle={getDrawHandleStyle}
				getFeatureStyle={getDrawFeatureStyle}
			/> */}
		</MapGL>
	);
}
