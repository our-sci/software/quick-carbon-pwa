import React, { useState } from 'react';
import ReactMapboxGL from 'react-mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

const MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;

export default function MBMap() {
	const Map = ReactMapboxGL({ accessToken: MAPBOX_TOKEN });
	return (
		// <Map style="mapbox://styles/mapbox/satellite-v9" />
		<Map 
			style="mapbox://styles/mapbox/satellite-v9"
			// style="mapbox://styles/mapbox/streets-v9" 
			containerStyle={{ height: '500px', width: '375px' }}
		>
		</Map>
	);
}
