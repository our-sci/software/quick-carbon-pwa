import React, { useState, useEffect, useRef, useContext } from 'react';
import MapboxGL, { Map, GeolocateControl } from 'mapbox-gl';
// import MapboxGL, { Map, GeolocateControl } from './mapbox-gl-dev';
import 'mapbox-gl/dist/mapbox-gl.css';
import PropTypes from 'prop-types';

import Geocoder from '@mapbox/mapbox-gl-geocoder';
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';

import Draw from '@mapbox/mapbox-gl-draw';
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css';
import { bbox } from '@turf/turf';
import { fieldFeaturesSchema, markersSchema } from 'components/FieldCreate/schemas';
import environmentIcon from 'assets/images/icons/environment-fill.png';
import circleIcon from 'assets/images/icons/circle-fill.png';

import PositionContext, { PositionContextProvider } from 'contexts/PositionContext';
import copyPosition from 'utils/copyPosition';

// import EventEmitter from 'events';

// import light9Modified from 'data/light9Modified';


const MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;


function addDrawInstanceToEvent(fn, drawRef) {
	return (ev) => fn({ ...ev, drawInstance: drawRef.current });
}

const initialState = {
	lng: -122.4376,
	lat: 37.7577,
	zoom: 0,
};


export default function MapboxGLMapSearchDraw({
	dragPan,
	drawCreateHandler,
	drawUpdateHandler,
	drawDeleteHandler,
	// setDrawInstance,
	features,
	markers,
	customMarkers,
	popups,
	hasDrawControl,
	hasGeocoderControl,
	hasGeolocateControl,
	height,
	width,
	eventListeners,
	geolocateCallback,
}) {
	const [mapState, setMapState] = useState(initialState);
	const mapInstanceRef = useRef();
	const mapContainer = useRef();
	const geocoderInstanceRef = useRef();
	const drawInstanceRef = useRef();
	const geolocateInstanceRef = useRef();
	const [positionState, setPositionState] = useContext(PositionContext);


	const styles = {
		width,
		// height: 'calc(100vh - 80px)',
		height,
	};

	useEffect(() => {
		function addGeocoderControl(geocoderInstanceRef) {
			geocoderInstanceRef.current = new Geocoder({
				accessToken: MAPBOX_TOKEN,
				// mapboxgl: mapInstanceRef.current,
				mapboxgl: MapboxGL,
				marker: false,
			});
			mapInstanceRef.current.addControl(geocoderInstanceRef.current);
		}

		function loadIcons() {
			mapInstanceRef.current.loadImage((environmentIcon), (error, image) => {
				if (error) throw error;
				mapInstanceRef.current.addImage('pin', image, { sdf: true });
			});
			mapInstanceRef.current.loadImage((circleIcon), (error, image) => {
				if (error) throw error;
				mapInstanceRef.current.addImage('circle', image, { sdf: true });
			});
		}

		function addGeolocateControl(geolocateInstanceRef) {
			// Might not need this as a ref
			geolocateInstanceRef.current = new GeolocateControl({
				positionOptions: {
					enableHighAccuracy: true,
				},
				fitBoundsOptions: {
					maxZoom: 20,
				},
				trackUserLocation: true,
				// trackUserLocation: false,
			});
			mapInstanceRef.current.addControl(geolocateInstanceRef.current);
			// console.log('initial geolocate ref', geolocateInstanceRef.current)
			// Copy original _onSuccess for monkey-patching
			// geolocateInstanceRef.current._onSuccessOriginal = geolocateInstanceRef.current._onSuccess;
			// geolocateInstanceRef.current.on('geolocate', (ev) => console.log(ev));
			// geolocateInstanceRef.current.on('geolocate', (ev) => setPositionState(ev));
			geolocateInstanceRef.current.on('geolocate', (ev) => setPositionState(copyPosition(ev)));
			// geolocateInstanceRef.current.on('geolocate', (ev) => requestAnimationFrame(() => geolocateCallback(ev)));
			// geolocateInstanceRef.current.on('geolocate', geolocateCallback);
			// geolocateInstanceRef.current.on('geolocate', geolocateCallback);
			// geolocateInstanceRef.current.on('geolocate', (ev) => console.log(ev));
			// geolocateInstanceRef.current._onSuccess = (...args) => {
			// 	geolocateInstanceRef.current._onSuccessOriginal(...args);
			// 	// calling fire() from here causes error
			// 	// mapInstanceRef.current.fire('geolocate.geolocate', ...args);
			// 	// console.log('geolocate event');
			// 	// geolocateCallback && geolocateCallback(...args);
				
			// };
			// mapInstanceRef.current.on('geolocate.geolocate', geolocateCallback);


		}

		function addDrawControl(drawInstanceRef) {
			drawInstanceRef.current = new Draw({
				displayControlsDefault: false,
				controls: {
					polygon: true,
					trash: true,
				},
			});
			// setDrawInstance(drawInstanceRef);
			mapInstanceRef.current.addControl(drawInstanceRef.current);

			mapInstanceRef.current.on('draw.create', addDrawInstanceToEvent(drawCreateHandler, drawInstanceRef));
			mapInstanceRef.current.on('draw.update', addDrawInstanceToEvent(drawUpdateHandler, drawInstanceRef));
			mapInstanceRef.current.on('draw.delete', addDrawInstanceToEvent(drawDeleteHandler, drawInstanceRef));

			// // mapInstanceRef.current.on('load', (ev) => {
			if (features.features.length > 0 && !!features.features[0].id) {
				drawInstanceRef.current.add(features.features[0]);
			}
			// // });
		}

		function mapMoveHandler() {
			const { lng, lat } = mapInstanceRef.current.getCenter();
			const zoom = mapInstanceRef.current.getZoom().toFixed(2);
			setMapState({ ...mapState, lng, lat, zoom });
		}

		// fieldFeaturesSchema.isValidSync(features.features)
		const bounds = fieldFeaturesSchema.isValidSync(features.features)
			? bbox(features.features[0])
			: null;

		function initializeMap() {
			MapboxGL.accessToken = MAPBOX_TOKEN;
			mapInstanceRef.current = new Map({
				container: mapContainer.current,
				center: [mapState.lng, mapState.lat],
				bounds,
				fitBoundsOptions: { padding: { top: 50, bottom: 50, left: 50, right: 50 }},
				zoom: mapState.zoom,
				// style: 'mapbox://styles/mapbox/streets-v9',
				// style: light9Modified,
				// style: 'mapbox://styles/wgardiner/ck0v42rbk1usl1ckqbkfo3a0h'
				style: 'mapbox://styles/mapbox/satellite-v9',
				// style: {
				// 	"version": 8,
				// 	"name": "Quick Carbon",
				// 	"sources": {
				// 		"mapbox-streets": {
				// 			"type": "vector",
				// 			"url": "mapbox://mapbox.mapbox-streets-v6"
				// 		}
				// 	}
				// }

			});
			mapInstanceRef.current.on('move', mapMoveHandler);

			
			loadIcons();
		}

		if (!mapInstanceRef.current) {
			// Map Instance does not exist, create it
			initializeMap();
		}

		if (!dragPan) {
			mapInstanceRef.current.dragPan.disable();
		}

		hasGeocoderControl && addGeocoderControl(geocoderInstanceRef);
		hasDrawControl && addDrawControl(drawInstanceRef);
		hasGeolocateControl && addGeolocateControl(geocoderInstanceRef);

		if (!hasDrawControl) {
			mapInstanceRef.current.on('load', (ev) => {
				mapInstanceRef.current.addLayer({
					id: 'features',
					// type: 'fill',
					type: 'line',
					source: {
						type: 'geojson',
						data: features,
					},
					paint: {
						// 'fill-color': '#fbb03b',
						// 'fill-outline-color': '#fbb03b',
						// 'fill-opacity': 0.1,
						// 'fill-color': '#000000',
						// 'fill-opacity': 0.5,
						// 'fill-outline-color': 'white',
						// 'line-color': '#0000ff',
						// 'line-width': 2,
						// 'line-opacity': 0.5,
						// 'line-color': '#fbb03b',
						'line-color': '#3bb2d0',
						// 'line-dasharray': [0.2, 2],
						'line-width': 2,
					},
					layout: {
						'line-join': 'round',
						'line-cap': 'round',
					},
				});
				
			});


		}

		return () => mapInstanceRef.current.remove();
	}, []);

	useEffect(() => {
		// try {
		// 	fieldFeaturesSchema.validateSync(features.features)
		// } catch (err) {
		// 	console.log('error', err);
		// }
		if (
			features
			&& features.features
			&& fieldFeaturesSchema.isValidSync(features.features)
		) {
			const bounds = bbox(features.features[0]);
			mapInstanceRef.current.fitBounds(bounds, { 
				padding: { top: 50, bottom: 50, left: 50, right: 50 },
			});
		}
	}, [features]);


	useEffect(() => {
		if (
			markers
			&& markers.features
			&& markersSchema.isValidSync(markers.features)
		) {
			// const bounds = bbox(markers);
			// mapInstanceRef.current.fitBounds(bounds);
			const markerLayer = mapInstanceRef.current.getLayer('markers');
			if (markerLayer) {	
				// const surveyLocationMarkers = { ...markers, features: markers.features.filter(
				// 	(marker) => marker.properties.type === 'SURVEY_LOCATION_MARKER'
				// )};
				console.log('marker layer exists');
				console.log('setting data')
				mapInstanceRef.current.getSource('markers').setData(markers);
			} else {
				if (mapInstanceRef.current && mapInstanceRef.current.isStyleLoaded()) {
					mapInstanceRef.current.addLayer({
						id: 'markers',
						type: 'symbol',
						source: {
							type: 'geojson',
							data: markers,
							cluster: false,
						},
						layout: {
							// 'icon-image': 'marker-15',
							'icon-image': [
								'match',
								['get', 'markerType'],
								'LOCATION_INCOMPLETE', 'circle-stroked-11',
								'LOCATION_COMPLETE', 'circle-11',
								'LOCATION_COLOR_INTERPOLATE', 'circle',
								'SURVEY_LOCATION_MARKER', 'circle-11',
									/* other */ 'circle-11',
							],
							'icon-size': 1,
							// 'icon-offset': [0, -20],
							// 'icon-image': 'pin',
							// 'icon-color': '#ff0000',
							// 'text-field': '1',
							'text-field': ['get', 'label'],
							'text-offset': [0, -0.75],
							'text-allow-overlap': true,
							'icon-allow-overlap': true,
							// 'text-font': ['Font Awesome 5 Free'],
							// 'text-color': '#fff',
							// 'text-field': ['format',
							// 	['get', 'label']
							// ],

						},
						paint: {
							// 'icon-color': '#ff00ff',
							'icon-color': [
								'match',
								['get', 'markerType'],
								'LOCATION_COLOR_INTERPOLATE', [
									'interpolate',
									['linear'],
									['get', 'value'],
									0,
									'green',
									0.5,
									'yellow',
									0.9,
									'rgb(230, 0, 0)',
								],
								/* other */ '#fff',
							],
							// 'text-color': '#ffffff',
							'text-halo-color': '#fff',
							'text-halo-width': 2,
						},
					});
				} else {
					console.log('set on load show markers');
					mapInstanceRef.current.on('load', (ev) => {
						console.log('setting markers');
						mapInstanceRef.current.addLayer({
							id: 'markers',
							type: 'symbol',
							source: {
								type: 'geojson',
								data: markers,
								cluster: false,
							},
							layout: {
								// 'icon-image': 'marker-15',
								'icon-image': [
									'match',
									['get', 'markerType'],
									'LOCATION_INCOMPLETE', 'circle-stroked-11',
									'LOCATION_COMPLETE', 'circle-11',
									'LOCATION_COLOR_INTERPOLATE', 'circle',
									'SURVEY_LOCATION_MARKER', 'circle-11',
									/* other */ 'circle-11',
								],
								'icon-size': 1,
								// 'icon-offset': [0, -20],
								// 'icon-image': 'pin',
								// 'icon-color': '#ff0000',
								// 'text-field': '1',
								'text-field': ['get', 'label'],
								'text-offset': [0, -0.75],
								'text-allow-overlap': true,
								'icon-allow-overlap': true,
								// 'text-font': ['Font Awesome 5 Free'],
								// 'text-color': '#fff',
								// 'text-field': ['format',
								// 	['get', 'label']
								// ],
	
							},
							paint: {
								// 'icon-color': '#ff00ff',
								'icon-color': [
									'match',
									['get', 'markerType'],
									'LOCATION_COLOR_INTERPOLATE', [
										'interpolate',
										['linear'],
										['get', 'value'],
										0,
										'green',
										0.5,
										'yellow',
										0.9,
										'rgb(230, 0, 0)',
									],
								/* other */ '#fff',
								],
								// 'text-color': '#ffffff',
								'text-halo-color': '#fff',
								'text-halo-width': 2,
							},
						});
					});
				}
			}
		}
	}, [markers]);

	const customMarkerRef = useRef();
	useEffect(() => {
		// customMarkers && markersSchema.validateSync(customMarkers.features);

		const customMarkersIsValid = customMarkers && customMarkers.features && markersSchema.isValidSync(customMarkers.features);
		if (customMarkersIsValid) {
			if (!customMarkerRef.current) {
				// customMarkers.forEach((customMarker) => {})
				const el = document.createElement('div');
				el.className = 'marker';
				// el.style = 'width: 30px; height: 30px; background-color: black; border-radius: 50%; opacity: 0.3; mix-blend-mode: multiply';
				el.style = 'width: 30px; height: 30px; background-color: white; border-radius: 50%; opacity: 0.5;';

				customMarkerRef.current = new MapboxGL.Marker(el)
					.setLngLat(customMarkers.features[0].geometry.coordinates)
					.addTo(mapInstanceRef.current);
			} else {
				customMarkerRef.current.setLngLat(customMarkers.features[0].geometry.coordinates);
			}
		}

		if (!customMarkersIsValid && customMarkerRef.current) {
			customMarkerRef.current.remove();
			customMarkerRef.current = null;
		}
		// return () => { 
		// 	customMarkerRef.current.remove();
		// 	customMarkerRef.current = null;
		// };
	}, [customMarkers]);

	const popupRef = useRef();
	useEffect(() => {
		if (popups && popups.features && markersSchema.isValidSync(popups.features)) {
			if (!popupRef.current) {

				popupRef.current = new MapboxGL.Popup()
					.setLngLat(popups.features[0].geometry.coordinates)
					.setHTML(`<div class="">${popups.features[0].properties.description}</div>`)
					.addTo(mapInstanceRef.current);
			} else {
				popupRef.current.setLngLat(popups.features[0].geometry.coordinates)
					.setHTML(`<div class="">${popups.features[0].properties.content}</div>`)
			}
			// const popupLayer = mapInstanceRef.current.getLayer('popups');
			// if (popupLayer) {

			// 	mapInstanceRef.current.getSource('popups').setData(popups);
			// } else {
			// 	mapInstanceRef.current.on('load', () => {

			// 	});
			// }
		}
	}, [popups]);

	useEffect(() => {
		eventListeners.forEach(({ type, listener, layerId = null }) => {
			if (layerId) {
				mapInstanceRef.current.on(type, layerId, listener);
			} else {
				mapInstanceRef.current.on(type, listener);
			}
		});
		return () => {
			eventListeners.forEach(({ type, listener, layerId = null }) => {
				if (layerId) {
					mapInstanceRef.current.off(type, layerId, listener);
				} else {
					mapInstanceRef.current.off(type, listener);
				}
			});
		};
	}, [eventListeners]);

	// useEffect(() => {
	// 	// HACK: geolocateInstanceRef.current is somehow getting reset to undefined
	// 	// ...let's resurrect it
	// 	const foundGeolocateControl = mapInstanceRef.current._controls.find((c) => c.hasOwnProperty('_geolocateButton'));
	// 	if (foundGeolocateControl) {
	// 		geolocateInstanceRef.current = foundGeolocateControl;
	// 	}

	// 	if (geolocateInstanceRef.current) {
	// 		geolocateInstanceRef.current._onSuccess = geolocateInstanceRef.current._onSuccessOriginal;
	// 		// geolocateInstanceRef.current._onSuccess = (...args) => {
	// 		// 	geolocateInstanceRef.current._onSuccessOriginal(...args);
	// 		// 	// calling fire() from here causes error
	// 		// 	// mapInstanceRef.current.fire('geolocate.geolocate', ...args);
	// 		// 	console.log('geolocate event fresh');
	// 		// 	// geolocateCallback && geolocateCallback(...args);
	// 		// };
	// 		// mapInstanceRef.current.on('geolocate.geolocate', geolocateCallback);

	// 	}
	// }, [geolocateCallback]);

	useEffect(() => {
		console.log('MAP: use effect: geolocate callback', geolocateCallback);

		if(mapInstanceRef.current) {

			// console.log('geolocate callback useeffect', geolocateInstanceRef.current);
		}
		// HACK: geolocateInstanceRef.current is somehow getting reset to undefined
		// ...let's resurrect it
		const foundGeolocateControl = mapInstanceRef.current._controls.find((c) => c.hasOwnProperty('_geolocateButton'));
		if (foundGeolocateControl) {
			geolocateInstanceRef.current = foundGeolocateControl;
			geolocateInstanceRef.current.on('geolocate', geolocateCallback);
		}
		// return () => geolocateInstanceRef.current.off('geolocate', geolocateCallback);
	}, [geolocateCallback]);

	return (
		<div
			className="map"
			ref={mapContainer}
			style={styles}
		/>
	);
}

MapboxGLMapSearchDraw.defaultProps = {
	dragPan: true,
	hasDrawControl: false,
	hasGeocoderControl: false,
	hasGeolocateControl: false,
	drawCreateHandler: (ev) => console.log(ev),
	drawUpdateHandler: (ev) => console.log(ev),
	drawDeleteHandler: (ev) => console.log(ev),
	features: { 
		type: 'FeatureCollection',
		features: [],
	},
	markers: null,
	customMarkers: null,
	popups: null,
	height: '500px',
	width: '375px',
	eventListeners: [],
};

MapboxGLMapSearchDraw.propTypes = {
	eventListeners: PropTypes.array,
	dragPan: PropTypes.bool,
	hasDrawControl: PropTypes.bool,
	hasGeolocateControl: PropTypes.bool,
	hasGeocoderControl: PropTypes.bool,
	drawCreateHandler: PropTypes.func,
	drawUpdateHandler: PropTypes.func,
	drawDeleteHandler: PropTypes.func,
	features: PropTypes.shape({
		type: PropTypes.string,
		features: PropTypes.array,
	}),
	markers: PropTypes.shape({
		type: PropTypes.string,
		features: PropTypes.array,
	}),
	customMarkers: PropTypes.shape({
		type: PropTypes.string,
		features: PropTypes.array,
	}),
	popups: PropTypes.shape({
		type: PropTypes.string,
		features: PropTypes.array,
	}),
	height: PropTypes.string,
	width: PropTypes.string,
};
