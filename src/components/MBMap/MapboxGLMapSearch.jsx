import React, { useState, useEffect, useLayoutEffect, useRef } from 'react';
import MapboxGL, { Map } from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';

import Geocoder from '@mapbox/mapbox-gl-geocoder';
import '@mapbox/mapbox-gl-geocoder/dist/mapbox-gl-geocoder.css';

const MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;

const styles = {
	width: '375px',
	// height: 'calc(100vh - 80px)',
	height: '500px',
};

const initialState = {
	lng: -122.4376,
	lat: 37.7577,
	zoom: 8,
};

export default function MapboxGLMapSearch() {
	const [mapState, setMapState] = useState(initialState);
	const mapInstanceRef = useRef();
	const mapContainer = useRef();
	const geocoderInstanceRef = useRef();

	useEffect(() => {
		function mapMoveHandler() {
			const { lng, lat } = mapInstanceRef.current.getCenter();
			const zoom = mapInstanceRef.current.getZoom().toFixed(2);
			setMapState({ ...mapState, lng, lat, zoom });
		}

		function initializeMap() {
			MapboxGL.accessToken = MAPBOX_TOKEN;
			mapInstanceRef.current = new Map({
				container: mapContainer.current,
				center: [mapState.lng, mapState.lat],
				zoom: 5,
				style: 'mapbox://styles/mapbox/streets-v9',
			});
			mapInstanceRef.current.on('move', mapMoveHandler);
		}

		if (!mapInstanceRef.current) {
			// Map Instance does not exist, create it
			initializeMap();
		}

		geocoderInstanceRef.current = new Geocoder({
			accessToken: MAPBOX_TOKEN,
			mapboxgl: mapInstanceRef.current,
		});

		mapInstanceRef.current.addControl(geocoderInstanceRef.current);

		console.log(mapInstanceRef.current);
		console.log(geocoderInstanceRef.current);

		return () => mapInstanceRef.current.remove();
	}, []);

	return (
		<div 
			className="map" 
			ref={mapContainer}
			style={styles}
		/>
	);
}
