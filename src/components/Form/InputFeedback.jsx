import React from 'react';
import classNames from 'classnames';

export default function InputFeedback({ error, className }) {
	const classes = classNames('input-feedback', className);
	return error
		? <div className={classes}>{error}</div>
		: null;
}
