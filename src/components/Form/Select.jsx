import React from 'react';
import classNames from 'classnames';
// import InputFeedback from 'components/Form/InputFeedback';
import { ErrorMessage } from 'formik';

export default function Select({
	field: { name, value, onChange, onBlur },
	form: { errors, touched, setFieldValue },
	id,
	label,
	type,
	disabled,
	children,
	className,
	inputClassName,
}) {
	const classes = classNames(['Select', 'field-group', className])
	const inputClasses = classNames(['select', 'mobile-display-block', 'width-100', inputClassName]);
	return (
		<div className={classes}>
			<label className="label mobile-display-block" htmlFor={id}>{label}</label>
			<select
				name={name}
				value={value}
				id={id}
				disabled={disabled}
				onChange={onChange}
				className={inputClasses}
			>
				{React.Children.map(children, (child) => {
					return React.cloneElement(child, {
						field: {
							value: child.props.value,
							// onChange: handleChange,
							// onBlur: handleBlur,
						},
					});
				})}
			</select>
			<ErrorMessage name={name} component="div" />
		</div>
	);
}
