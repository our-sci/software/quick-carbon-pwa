import React from 'react';
import classNames from 'classnames';
import InputFeedback from 'components/Form/InputFeedback';

export default function RadioButtonGroup({
	value,
	error,
	touched,
	id,
	label,
	className,
	children,
	disabled,
}) {
	const classes = classNames(
		'field-group',
		'position-relative',
		{
			'is-success': value || (!error && touched), // handle prefilled or user-filled
			'is-error': !!error && touched,
		},
		className,
	);

	const wrapperClasses = classNames('checkbox-group-wrapper', {
		error: error,
		disabled: disabled,
	});

	return (
		<div className={classes}>
			<fieldset className="fieldset">
				<legend className="legend">{label}</legend>
				<div className={wrapperClasses}>
					{/* {children} */}
					{React.Children.map(children, (child) => {
						return React.cloneElement(child, {
							disabled,
						});
					})}
				</div>
				{touched && <InputFeedback className="error message input-message" error={error} />}
			</fieldset>
		</div>
	);
}
