import React from 'react';
import classNames from 'classnames';
import InputFeedback from 'components/Form/InputFeedback';

export default function Checkbox({
	field: { name, value, onChange, onBlur },
	form: { errors, touched/*, setFieldValue */ },
	id,
	label,
	className,
	// ...props,
}) {
	const classes = classNames(className, 'Checkbox');
	return (
		<div className={classes} >
			<input
				type="checkbox"
				name={name}
				id={id}
				value={value}
				checked={value}
				onChange={onChange}
				onBlur={onBlur}
				className="checkbox-input"
			/>
			<label htmlFor={id}>{label}</label>
			{touched[name] && <InputFeedback error={errors[name]} />}
		</div>
	);
}
