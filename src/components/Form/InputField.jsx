import React from 'react';
import classNames from 'classnames';
// import InputFeedback from 'components/Form/InputFeedback';
import { ErrorMessage } from 'formik';

export default function InputField({
	field: { name, value, onChange, onBlur },
	form: { errors, touched, setFieldValue },
	id,
	label,
	type,
	disabled,
	autoFocus,
	// fieldValue,
}) {
	// setFieldValue(name, fieldValue);
	const inputClasses = classNames('input', type, 'mobile-display-block', 'width-100', {
		success: value || (!errors[name] && touched),
		error: !!errors[name] && touched,
	});
	return (
		<div className="InputField field-group position-relative">
			<label className="label mobile-display-block" htmlFor={id}>{label}</label>
			<input
				type={type}
				name={name}
				id={id}
				value={value}
				onChange={onChange}
				onBlur={onBlur}
				className={inputClasses}
				disabled={disabled}
				autoFocus={autoFocus}
			/>
			{/* {touched[name] && <InputFeedback error={errors[name]} />} */}
			<ErrorMessage className="message error input-message" name={name} component="div" />
		</div>
	);
}
