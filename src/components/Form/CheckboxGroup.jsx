import React from 'react';
import classNames from 'classnames';
import InputFeedback from 'components/Form/InputFeedback';

export default function CheckboxGroup({
	value,
	error,
	touched,
	label,
	className,
	children,
	onChange,
	onBlur,
	id,
}) {
	const classes = classNames(
		// 'input-field',
		'field-group',
		'position-relative',
		{
			'is-success': value || (!error && touched),
			'is-error': !!error && touched,
		},
		className,
	);

	const wrapperClasses = classNames('checkbox-group-wrapper', {
		error: error,
	});

	function handleChange(event) {
		const { target } = event;
		const valueArray = [...value] || [];

		if (target.checked) {
			valueArray.push(target.id);
		} else {
			valueArray.splice(valueArray.indexOf(target.id), 1);
		}

		onChange(id, valueArray);
	}

	function handleBlur(event) {
		onBlur(id, true);
	}

	return (
		<div className={classes}>
			<fieldset className="fieldset">
				<legend className="legend">{label}</legend>
				<div className={wrapperClasses}>
					{React.Children.map(children, (child) => {
						return React.cloneElement(child, {
							field: {
								value: value.includes(child.props.id),
								onChange: handleChange,
								onBlur: handleBlur,
							},
						});
					})}
				</div>
				{touched && <InputFeedback className="error message input-message" error={error} />}
			</fieldset>
		</div>
	);
}
