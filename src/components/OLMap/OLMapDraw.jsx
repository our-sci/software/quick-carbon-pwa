import React, {
	useEffect,
	useState,
	useRef,
	useContext,
} from 'react';

import AppContext, { AppContextProvider } from 'contexts/AppContext';

import { Map, View } from 'ol';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer';
import { OSM, Vector as VectorSource } from 'ol/source';
import { transform } from 'ol/proj';
import { defaults as defaultInteractions, Draw, Modify, Snap } from 'ol/interaction';
import 'ol/ol.css';
import styles from './OLMap.module.css';

export default function OLMapDraw() {
	const appContextValue = useContext(AppContext);
	console.log(appContextValue);


	const mapInstanceRef = useRef();
	const mapEl = useRef();
	const drawInstanceRef = useRef();
	const [mapState, setMapState] = useState({
		center: [0, 0],
		zoom: 1,
	});



	function addInteractions(mapInstance, drawSource) {
		const drawInstance = new Draw({
			source: drawSource,
			type: 'Polygon',
		});
		mapInstance.addInteraction(drawInstance);
		mapInstance.addInteraction(new Snap({ source: drawSource }));
		mapInstance.addInteraction(new Modify({ source: drawSource }));


		return drawInstance;
	}

	// create / destroy map
	useEffect(() => {
		if (!mapInstanceRef.current) {
			console.log(mapInstanceRef.current);
			// map instance does not exist, create a new one

			const drawSource = new VectorSource({ wrapX: false });
			// const drawVector = new VectorLayer({ source: drawSource });

			mapInstanceRef.current = new Map({
				target: mapEl.current,
				layers: [
					new TileLayer({ source: new OSM() }),
					new VectorLayer({ source: drawSource }),
				],
				view: new View({
					center: mapState.center,
					zoom: mapState.zoom,
				}),
				controls: [],
				interactions: defaultInteractions({
					dragPan: false,
					mouseWheelZoom: false,
					pinchZoom: false,
					pinchRotate: false,
				}),
			});

			drawInstanceRef.current = addInteractions(mapInstanceRef.current, drawSource);
		} else {
			// state updated, set view
			mapInstanceRef.current.setTarget(mapEl.current);
			mapInstanceRef.current.setView(new View({
				center: transform(mapState.center, 'EPSG:4326', 'EPSG:3857'),
				zoom: mapState.zoom,
			}));
			console.log(mapInstanceRef.current);
		}
		return () => mapInstanceRef.current.setTarget(null);
	}, [mapState]);

	

	return (
		<div className="OLMap">
			<div className={styles['ol-map-root']} ref={mapEl} />
			<button type="button" onClick={() => setMapState({ center: [-84.512016, 39.103119], zoom: 10 })}>
				cincy
			</button>
			<button type="button" onClick={() => appContextValue[1]({...appContextValue[0], blah: 'blurg'})}>
				change state
			</button>
			<p>use alt key to delete vertex in polygon</p>
		</div>
	);
}
