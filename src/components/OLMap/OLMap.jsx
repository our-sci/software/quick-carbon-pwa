import React, { useEffect, useState, useRef, useContext } from 'react';

import AppContext, { AppContextProvider } from 'contexts/AppContext';

import { Map, View } from 'ol';
import TileLayer from 'ol/layer/Tile';
import OSM from 'ol/source/OSM';
import { transform } from 'ol/proj';
import { defaults as defaultInteractions } from 'ol/interaction';
import 'ol/ol.css';
import styles from './OLMap.module.css';

export default function OLMap() {
	const appContextValue = useContext(AppContext);
	console.log(appContextValue);


	const mapInstanceRef = useRef();
	const mapEl = useRef();
	const [mapState, setMapState] = useState({
		center: [0, 0],
		zoom: 1,
	});

	// create / destroy map
	useEffect(() => {
		if (!mapInstanceRef.current) {
			console.log(mapInstanceRef.current);
			// map instance does not exist, create a new one
			mapInstanceRef.current = new Map({
				target: mapEl.current,
				layers: [
					new TileLayer({ source: new OSM() }),
				],
				view: new View({
					center: mapState.center,
					zoom: mapState.zoom,
				}),
				controls: [],
				interactions: defaultInteractions({
					dragPan: false,
					mouseWheelZoom: false,
					pinchZoom: false,
					pinchRotate: false,
				})
			});
		} else {
			// state updated, set view
			mapInstanceRef.current.setTarget(mapEl.current);
			mapInstanceRef.current.setView(new View({
				center: transform(mapState.center, 'EPSG:4326', 'EPSG:3857'),
				zoom: mapState.zoom,
			}));
			console.log(mapInstanceRef.current);
		}
		return () => mapInstanceRef.current.setTarget(null);
	}, [mapState]);

	return (
		<div className="OLMap">
			<div className={styles['ol-map-root']} ref={mapEl} />
			<button type="button" onClick={() => setMapState({ center: [-84.512016, 39.103119], zoom: 10 })}>
				cincy
			</button>
			<button type="button" onClick={() => appContextValue[1]({...appContextValue[0], blah: 'blurg'})}>
				change state
			</button>
		</div>
	);
}
