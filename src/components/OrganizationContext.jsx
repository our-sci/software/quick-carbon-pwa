import React, { createContext } from 'react';

const organizations = [
	{
		id: 1,
		name: 'Our Sci',
	},
	{
		id: 2,
		name: 'Snapp Lab',
	},
	{
		id: 3,
		name: 'Real Food Campagin',
	},
];

const OrganizationContext = createContext({ organization: null });

function OrganizationContext() {};