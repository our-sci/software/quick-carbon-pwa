import React from 'react';
import Icon from '@ant-design/icons-react';
import classNames from 'classnames';
import SampleButton from './SampleButton';

function DistanceAndAccuracy({
	distance, 
	accuracy,
}) {
	return (
		<div className="distance-and-accuracy display-inline-block float-right">

			{/* Distance:&nbsp; */}
			{(distance * 1000).toFixed(2)}m
			&nbsp;± {accuracy}m
		</div>
	);
}

function RangeAndTypeIndicator({
	isWithinRange,
	type,
}) {
	return (
		<span style={{ marginLeft: '5px' }}>
			{!isWithinRange && type === 'AUTO' && (
				<Icon style={{ color: '', verticalAlign: '-0.25em' }} type="heat-map-o" />
			)}
			{isWithinRange &&  type === 'AUTO' && (
				<Icon style={{ color: '#21ba45', verticalAlign: '-0.25em' }} type="heat-map-o" />
			)}
			{!isWithinRange && type === 'SELECTED' && (
				<Icon style={{ color: '', verticalAlign: '-0.25em' }} type="flag-o" />
			)}
			{isWithinRange &&  type === 'SELECTED' && (
				<Icon style={{ color: '#21ba45', verticalAlign: '-0.25em' }} type="flag-o" />
			)}
			{/* {isWithinRange && <Icon style={{ color: '#21ba45' }} type="flag-fill" />} */}
		</span>
	);
}


//
// hollow: !sampleExists for Survey
// filled: sampleExists for Survey
// edit-icon: sample exists for Survey
// plus-icon: no sample exists for survey
// disabled (faded): !(isWithinRange || sampleExists)
// enabled: isWithinRange || sampleExists


export default function LocationInfo({
	location,
	distance,
	accuracy,
	depths,
	isWithinRange,
	onSampleCreateClick,
	type,
	hasGeolocation,
	locationSamples, // samples filtered to location
	// onClick,
}) {
	return (
		<div className="location-info">
			<div className="location-header">
				<div className="location-title display-inline-block">
					<strong>Marker {location.properties.label}</strong>
					{/* <span className="font-size-75">&nbsp;(
					{location.id}
					)</span> */}
					<RangeAndTypeIndicator type={type} isWithinRange={isWithinRange} />
				</div>
				{distance && accuracy && (
					<DistanceAndAccuracy distance={distance} accuracy={accuracy} />
				)}
				<br />
			</div>
			<div>
				Sample{depths.length > 1 && 's'}
			</div>
			<div className="text-align-center padding-10-0 margin-bottom-10">
				{depths.map((depth) => (
					<SampleButton 
						depth={depth} 
						isWithinRange={isWithinRange} 
						key={depth.handle}
						locationSamples={locationSamples}
						locationId={location.id}
						// onClick={() => onSampleCreate(location.id, depth.handle)}
						onSampleCreateClick={onSampleCreateClick}
						hasGeolocation={hasGeolocation}
					/>
				))}
			</div>
			{/* <div className="text-align-center">
				<button
					type="button"
					className="button primary icon"
					disabled={!isWithinRange}
					onClick={() => onSampleCreate(location.id)}
				>
					<Icon type="plus-o" />&nbsp;
					New Sample
				</button>
			</div> */}

		</div>
	)
}
