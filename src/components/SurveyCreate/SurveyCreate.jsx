import React, { useState, useContext, useEffect } from 'react';
import MapboxGLMap from 'components/MBMap/MapboxGLMapSearchDraw';
import AppContext from 'contexts/AppContext';
import LocationInfo from 'components/SurveyCreate/LocationInfo';
import LocationInfoWrapper from './LocationInfoWrapper';
import ProgressView from './ProgressView';
import EnableGeolocationMessage from 'components/SurveyCreate/EnableGeolocationMessage';
import ObjectID from 'bson-objectid';
import Icon from '@ant-design/icons-react';
import { Link, Prompt, useHistory } from 'react-router-dom';
import download from 'utils/download';
import copyToClipboard from 'utils/copyToClipboard';
import submitToOurSci from 'utils/submitToOurSci';



const ACCURACY_THRESHOLD = process.env.REACT_APP_ACCURACY_THRESHOLD;
const DISTANCE_THRESHOLD = process.env.REACT_APP_DISTANCE_THRESHOLD;
const MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;
const BACKEND_BASE_URL = process.env.REACT_APP_BACKEND_URL;
window.ACCURACY_THRESHOLD = ACCURACY_THRESHOLD;
window.DISTANCE_THRESHOLD = DISTANCE_THRESHOLD;



function getField(id, fields) {
	return fields.find((field) => String(field.id) === id);
}

function getLocations(id, locationCollections, ) {
	if (id) {
		return locationCollections.find(
			(location) => String(id) === String(location.id),
		);
	}
}

function getLocation(id, locations) {
	return locations.features.find((l) => l.properties.id === id);
}

function featureCollection(features) {
	return {
		type: 'FeatureCollection',
		features,
	};
}

function getDepth(id, depths) {
	return depths.find(
		(depthObj) => id === depthObj.handle || id === depthObj.id,
	);
}

function getDepths(ids, depths) {
	return ids.map((depthId) => getDepth(depthId, depths))
		.sort((a, b) => a.min_depth - b.min_depth);
}

function getCustomMarkers(selectedLocation, ) {
	if (selectedLocation) {
		return featureCollection(selectedLocation);
	}
	return featureCollection([]);
}

function getSamplesForLocation(locationId, sampleCollection) {
	if (locationId && sampleCollection) {
		// console.log(sampleCollection.map(s => s.properties));
		return sampleCollection.filter((sample) => sample.properties.locationId === locationId);
	}
	return [];
}

function getNumberOfCompletedLocations(locations, samples, depthIds) {
	const numberOfRequiredDepths = depthIds.length;
	const locationSamples = locations.map((location) => {
		const samplesForLocation = getSamplesForLocation(location.id, samples);
		if (samplesForLocation.length < numberOfRequiredDepths) {
			return 'LOCATION_INCOMPLETE';
		}
		return 'LOCATION_COMPLETE';
	});
	return locationSamples.filter((ls) => ls === 'LOCATION_COMPLETE').length;
}

function getMarkersWithSamples(locations, samples, depthIds) {
	const numberOfRequiredDepths = depthIds.length;
	if (locations) {
		const sampleMarkers = locations.map((location) => {
			const samplesForLocation = getSamplesForLocation(location.id, samples);
			let markerType;
			if (samplesForLocation.length < numberOfRequiredDepths) {
				markerType = 'LOCATION_INCOMPLETE';
			} else {
				markerType = 'LOCATION_COMPLETE';
			}
			return { 
				...location,
				properties: {
					...location.properties,
					markerType,
	
				},
			};
		});
		return featureCollection(sampleMarkers);
	}
	return {};
}

function ReviewMapToggleButton({ showReview, setShowReview }) {
	return !showReview ? (
		<button
			type="button"
			className="button icon-only no-bg position-absolute right-10 vertical-center-top-transform"
			onClick={() => setShowReview(true)}
		>
			<Icon type="ordered-list-o" />
		</button>
	) : (
		<button
			type="button"
			className="button icon-only no-bg position-absolute right-10 vertical-center-top-transform"
			onClick={() => setShowReview(false)}
		>
			<Icon type="global-o" />
		</button>
	);
}

function CompletionWidget({
	samples,
	numberOfRequiredSamples,
	numberOfLocations,
	numberOfCompletedLocations,
	// locations,
	// modalIsVisible,
	// onModalClose,
	// depths,
	// onSampleCreate,
}) {
	return (
		<div className="completion-widget font-size-75">
			
			<div className="strong">
				Progress
			</div>
			<div>
				{samples.length} / {numberOfRequiredSamples} samples
			</div>
			<div>
				{numberOfCompletedLocations} / {numberOfLocations} locations
			</div>
		</div>
	);
}



export default function SurveyCreate({
	fieldId,
}) {
	const [appState, setAppState] = useContext(AppContext);
	const [state, setState] = useState({
		sampleCreateDepthId: null,
		sampleCreateId: null,
		surveyId: ObjectID().str,
		startedAt: Date.now(),
	});
	const [selectedLocationId, setSelectedLocationId] = useState(null);
	const [samples, setSamples] = useState([]);
	const [showReview, setShowReview] = useState(false);
	const [isSubmitting, setIsSubmitting] = useState(false);
	const [submissionErrors, setSubmissionErrors] = useState([]);

	const history = useHistory();

	// console.log(selectedLocationId);

	
	const field = getField(fieldId, appState.fields);

	// const [locations, setLocations] = useState([]);
	// const [sampleMarkers, setSampleMarkers] = useState({});
	const dataIsLoaded = field && field.properties && field.properties.surveyLocations;
	// if (dataIsLoaded) {
	// 	setLocations(getLocations(field.properties.surveyLocations, appState.surveyLocations));
	// 	setSampleMarkers(getMarkersWithSamples(locations.features, samples, field.properties.depths));
	// }

	const locations = (dataIsLoaded && getLocations(field.properties.surveyLocations, appState.surveyLocations)) || {};
	const selectedLocation = selectedLocationId && getLocation(selectedLocationId, locations);
	// console.log('locations', locations);
	const customMarkers = getCustomMarkers([selectedLocation]);
	// const depths = getDepths(field.properties.depths, appState.soilDepths);
	// debugger;
	const sampleMarkers = (dataIsLoaded && getMarkersWithSamples(locations.features, samples, field.properties.depths)) || {};
	console.log('sampleMarkers', sampleMarkers);
	const depths = (dataIsLoaded && getDepths(field.properties.depths, appState.soilDepths)) || [];
	
	
	useEffect(() => {
		if (samples.length > 0) {
			window.onbeforeunload = (ev) => { 
				ev.preventDefault();
				ev.returnValue = 'Are you sure you want to leave? You have unsaved samples.';
			}
		}
		return () => {
			window.onbeforeunload = undefined;
		};
	}, [samples]);



	function handleSampleCreate(sample) {
		// merge in sample, prevent duplicates by checking ids
		const sampleExistsIndex = samples.findIndex((s) => s.id === sample.id);
		let nextSamples;
		if (sampleExistsIndex > -1) {
			console.log('sample exists');
			nextSamples = [...samples];
			nextSamples.splice(sampleExistsIndex, 1, sample);
			// debugger;
			// nextSamples = [
				// 	...samples.slice(0, sampleExistsIndex),
				// 	...samples.slice(sampleExistsIndex, samples.length),
				// 	sample,
				// ];

				
			} else {
				console.log('sample does not exist');
			nextSamples = [
				...samples,
				sample,
			];
		}
		setSamples(nextSamples);
	}



	function handleMapClick(ev) {
		const features = ev.target.queryRenderedFeatures(ev.point);
		// console.log('handle map click', features, features[0], selectedLocationId);
		// console.log('handle map click', features, features[0], selectedLocationId);
		if (features && features[0]) {
			if (features[0].properties.id !== selectedLocationId) {
				// console.log('first case')
				// setState({ ...state, selectedLocationId: features[0].properties.id });
				setSelectedLocationId(features[0].properties.id);
			} else {
				// console.log('else case')
				// setState({ ...state, selectedLocationId: null });
				setSelectedLocationId(null);
			}

		}
	}

	function handleCopySamples() {
		copyToClipboard(
			JSON.stringify({ 
				type: 'FeatureCollection', 
				features: samples,
				properties: {
					surveyId: state.surveyId,
					fieldId,
				},
			}),
		);
	}

	function handleDownloadSamples() {
		download(
			JSON.stringify({ 
				type: 'FeatureCollection', 
				features: samples,
				properties: {
					surveyId: state.surveyId,
					fieldId,
				},
			}),
			'samples.geojson',
			'application/geo+json',
		);
	}

	async function handleSubmitSamples() {
		setIsSubmitting(true);
		setSubmissionErrors([]);
		const errors = [];

		// POST array of samples to Sample endpoint
		// TODO: should I just switch to using _id instead of id internally?
		const remappedSamples = samples.map((s) => ({ ...s, _id: s.id }));

		try {
			const response = await fetch(`${BACKEND_BASE_URL}/sample`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify(remappedSamples),
			});
			const jsonResponse = await response.json();
			console.log('handleSubmitSamples response', jsonResponse);
			
		} catch (error) {
			errors.push(error);
			console.log(error);
		}

		// TODO: make timestamps dates accurate.
		try {
			const surveyResponse = await fetch(`${BACKEND_BASE_URL}/samplingevent`, {
				method: 'POST', 
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					_id: state.surveyId,
					startedAt: Date.now(),
					modifiedAt: Date.now(),
					completedAt: Date.now(),
					samples: samples.map((sample) => sample.id),
				}),
			});
			const surveyJsonResponse = await surveyResponse.json();
		} catch (error) {
			errors.push(error);
			console.log(error);
		}

		try {
			const response = await fetch(`${BACKEND_BASE_URL}/field/${fieldId}`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					...field,
					properties: {
						...field.properties,
						modifiedAt: Date.now(),
						samplingEvents: [...new Set([
							...field.properties.samplingEvents,
							state.surveyId,
						])],
					},
				})
			})
		} catch (error) {
			errors.push(error);
			console.log(error);
		}

		// const oursciResponse = await submitToOurSci(
		// 	{ field }, 
		// 	{ id: field.id, createdAt: field.properties.createdAt }, 
		// 	'field',
		// );

		try {
			const oursciSamplesResponse = await Promise.all(samples.map((sample) => submitToOurSci(
				{ sample, survey: { id: state.surveyId } },
				{ id: sample.id, createdAt: sample.properties.timestamp },
				'sample',
			)));
			console.log(oursciSamplesResponse);
			
		} catch (error) {
			errors.push(error);
			console.log(error);
		}

		// await submitToOurSci(
		// 	{ field }, 
		// 	{ id: field.id, createdAt: field.properties.createdAt }, 
		// 	'field',
		// );
		try {
			const oursciSurveyResponse = await submitToOurSci(
				{ field, survey: { id: state.surveyId }},
				{ id: state.surveyId, createdAt: state.createdAt }, 
				'survey',
			);
			console.log('submit to oursci done', oursciSurveyResponse);
		} catch (error) {
			console.log(error);
			errors.push(error);
		}
		
		setSubmissionErrors(errors);
		setIsSubmitting(false);
		history.push(`/field/${fieldId}`);
	}

	return (
		<div className="survey-create">
			<header className="page-header text-align-center position-relative">
				<Link to="/field" className="position-absolute left-10 vertical-center-top-transform">
					<button type="button" className="button icon-only circle no-bg">
						<Icon type="arrow-left-o" />
					</button>
				</Link>
				<ReviewMapToggleButton showReview={showReview} setShowReview={setShowReview} />
				<h1 className="page-title">Survey Create</h1>
				<div className="page-subtitle">&nbsp;</div>
			</header>
			{ dataIsLoaded && !showReview && (
				<>
					<div className="top position-relative">
						<MapboxGLMap
							features={{ type: 'FeatureCollection', features: [field] }}
							// markers={locations}
							markers={sampleMarkers}
							customMarkers={customMarkers}
							// popups={popups}
							height="calc(100vh - 300px)"
							width="100%"
							hasGeolocateControl
							// geolocateCallback={geolocateListener}
							eventListeners={[
								{
									type: 'click',
									listener: handleMapClick,
									layerId: 'markers',
								},
							]}
						/>
						<EnableGeolocationMessage
							className="position-absolute bottom-0"
						/> 
					</div>
					<div className="bottom padding-10">
						{selectedLocation ? (
							<LocationInfoWrapper
								location={selectedLocation}
								fieldId={fieldId}
								// handleSampleCreateClick={handleSampleCreateClick}
								handleSampleCreate={handleSampleCreate}
								samples={samples}
								surveyId={state.surveyId}
							/>
						) : (
							<div className="margin-top-10 margin-bottom-10">
								Select a location to begin sampling
							</div>
						)}
						{samples.length > -1 && (
							<>
								
								<div className="flex justify-content-space-between">
									
									<CompletionWidget 
										samples={samples}
										numberOfRequiredSamples={locations.features.length * field.properties.depths.length}
										// numberOfDepths={field.properties.depths.length}
										numberOfLocations={locations.features.length}
										numberOfCompletedLocations={getNumberOfCompletedLocations(locations.features, samples, field.properties.depths)}
										locations={locations}
										depths={depths}
										onSampleCreate={handleSampleCreate}
									/>
									<div className="margin-top-25">
										<button type="button" className="button circle icon-only" onClick={handleDownloadSamples}>
											<Icon type="download-o" />
										</button>
										<button type="button" className="button circle icon-only" onClick={handleCopySamples}>
											<Icon type="copy-o" />
										</button>
									</div>
								</div>
								

								<button 
									type="button" 
									className="button primary margin-top-10 margin-l-r-auto display-block"
									onClick={handleSubmitSamples}
									disabled={isSubmitting}
								>
									{isSubmitting ? (
										<div className="position-relative">
											<span>Submit Survey</span>
											<Icon 
												type="loading-o"
												className="anticon-spin position-absolute"
												style={{ left: '50%', top: 0, transform: 'translateX(-50%)', zIndex: 1 }}
											/>
										</div>
									) : (
										<span>Submit Survey</span>
									)}
								</button>
								{submissionErrors && submissionErrors.length > 0 && (
									<div className="message error submission-errors">
										An error occured with your submission: <br/>
										{submissionErrors}
									</div>
								)}
							</>
						)}

					</div>
				</>
			)}
			{ dataIsLoaded && showReview && (
				<div className="text-align-center">
					<ProgressView
						locations={locations}
						samples={samples}
						depths={depths}
						onSampleCreate={handleSampleCreate}
						surveyId={state.surveyId}
						setSelectedLocationId={setSelectedLocationId}
						setShowReview={setShowReview}
					/>
					
				</div>
			)}
			{ !dataIsLoaded && (
				<div className="text-align-center">
					<Icon type="loading-o" className="anticon-spin" />
				</div>
			)}

			<Prompt
				when={samples.length > 0}
				message="You have unsaved changes are you sure you want to leave?"
			/>
			
		</div>
	);
}
