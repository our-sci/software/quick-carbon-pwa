import React, { useContext } from 'react';
import classNames from 'classnames';
import PositionContext from 'contexts/PositionContext';


export default function EnableGeolocationMessage({
	showTitle,
	className,
}) {
	const [positionState, setPositionState] = useContext(PositionContext);
	const hasGeolocation = !!positionState;

	const classes = classNames(['message', 'warn', className]);
	// return (
	// 	// <div className="message info">
	// 	<div className={classes}>
	// 		{showTitle && (
	// 			<strong className="display-block">Start Tracking</strong>
	// 		)}

	// 		Please enable geolocation by clicking the
	// 		<div className="geolocate-icon" />
	// 		button on the map
	// 	</div>
	// );
	return !hasGeolocation && (
		<div 
			className="position-absolute"
			style={{
				top: '8px',
				right: '47px',
			}}
		>
			<div 
				className="position-relative message warn tool-tip-right"
			>
				Please enable geolocation
			</div>
		</div>
	)
}
