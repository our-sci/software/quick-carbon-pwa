import React, { useState, useContext } from 'react';
import MapboxGLMap from 'components/MBMap/MapboxGLMapSearchDraw';
import AppContext from 'contexts/AppContext';
import PositionContext from 'contexts/PositionContext';
import LocationInfo from 'components/SurveyCreate/LocationInfo';
import { distance } from '@turf/turf';
import { point } from '@turf/helpers';
import SampleCreateModal from 'components/SampleCreate/SampleCreateModal';



const ACCURACY_THRESHOLD = process.env.REACT_APP_ACCURACY_THRESHOLD;
const DISTANCE_THRESHOLD = process.env.REACT_APP_DISTANCE_THRESHOLD;
const MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;
window.ACCURACY_THRESHOLD = ACCURACY_THRESHOLD;
window.DISTANCE_THRESHOLD = DISTANCE_THRESHOLD;

function getField(id, fields) {
	return fields.find((field) => String(field.id) === id);
}

function getDepth(id, depths) {
	return depths.find(
		(depthObj) => id === depthObj.handle || id === depthObj.id,
	);
}

function getDepths(ids, depths) {
	return ids.map((depthId) => getDepth(depthId, depths))
		.sort((a, b) => a.min_depth - b.min_depth);
}

function getSample(id, samples) {
	if (!samples) {

		debugger;
	}
	return samples.find((s) => s.id === id);
}

function getSamplesForLocation(locationId, sampleCollection) {
	if (locationId && sampleCollection) {
		return sampleCollection.filter((sample) => sample.properties.locationId === locationId);
	}
	return [];
}


function getDistanceIsWithinValidRange(distanceToPoint, accuracy) {
	return accuracy < ACCURACY_THRESHOLD
		&& distanceToPoint * 1e3 < DISTANCE_THRESHOLD;
}

export default function LocationInfoWrapper({
	location,
	fieldId,
	handleSampleCreate,
	samples,
	surveyId,
}) {
	const [appState, setAppState] = useContext(AppContext);
	const [positionState, setPositionState] = useContext(PositionContext);
	const [modalIsVisible, setModalIsVisible] = useState(false);
	const [state, setState] = useState({
		sampleCreateDepthId: null,
		sampleCreateId: null,
	});

	const hasGeolocation = !!positionState;

	const field = getField(fieldId, appState.fields);
	const depths = getDepths(field.properties.depths, appState.soilDepths);
	const samplesForLocation = getSamplesForLocation(location.id, samples);


	const distanceToLocation = hasGeolocation && location
		&& distance(location.geometry, point([positionState.coords.longitude, positionState.coords.latitude]));
	const accuracy = hasGeolocation && positionState.coords.accuracy;
	const isWithinRange = getDistanceIsWithinValidRange(distanceToLocation, accuracy);



	function handleSampleCreateClick(locationId, depthId, id) {
		// console.log('sample create click')
		// const positionCopy = copyPosition(position);
		// console.log('handle sample create click', locationId, depthId, id);

		setState({
			...state,
			sampleCreateDepthId: depthId,
			sampleCreateId: id,
		});
		setModalIsVisible(true);
	}

	function handleSampleCreateClose() {
		setState({
			...state,
			sampleCreateDepthId: null,
			sampleCreateId: null,
		});
		setModalIsVisible(false);
	}

	function onSampleCreate(sample) {
		handleSampleCreate(sample);
		setModalIsVisible(false);
	}

	return (
		<>
			<LocationInfo
				// location={state.nearestLocation}
				// distance={state.nearestLocation.properties.distanceToPoint}
				location={location}
				// distance={'5'}
				// distance={5}
				hasGeolocation={hasGeolocation}
				distance={distanceToLocation}
				depths={depths}
				// isWithinRange={selectedIsWithinValidRange}
				isWithinRange={isWithinRange}
				// accuracy={hasGeolocation && position.coords.accuracy}
				accuracy={accuracy}
				onSampleCreateClick={handleSampleCreateClick}
				locationSamples={samplesForLocation}
				type="SELECTED"
			/>

			<SampleCreateModal
				onClose={handleSampleCreateClose}
				isVisible={modalIsVisible}
				location={location}
				depths={depths}
				surveyId={surveyId}
				isWithinValidRange={isWithinRange}
				onSubmit={onSampleCreate}
				depthId={state.sampleCreateDepthId}
				sampleId={!!state.sampleCreateId && getSample(state.sampleCreateId, samples).properties.sampleId}
				hiddenId={state.sampleCreateId}
				coordinates={state.sampleCreateId
					? getSample(state.sampleCreateId, samples).geometry.coordinates
					: !!positionState && [positionState.coords.longitude, positionState.coords.latitude]
				}
				timestamp={state.sampleCreateId
					? getSample(state.sampleCreateId, samples).properties.timestamp
					: !!positionState && positionState.timestamp
				}
			/>
		</>
	)

}
