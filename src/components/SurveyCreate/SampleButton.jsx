import React from 'react';
import classNames from 'classnames';
import Icon from '@ant-design/icons-react';

export default function SampleButton({
	depth,
	isWithinRange,
	locationSamples = [],
	// onClick,
	onSampleCreateClick,
	locationId,
	hasGeolocation,
}) {
	const samplesForDepth = locationSamples.filter(
		(sample) => sample.properties.depthId === depth.handle,
	);
	const sampleExists = samplesForDepth.length > 0;
	// console.log('has sample for depth', sampleExists, samplesForDepth, locationSamples);
	const classes = classNames(['button', 'font-size-75', 'icon'], {
		hollow: !sampleExists,
		outline: !sampleExists,
	});
	const iconType = sampleExists ? 'edit-o' : 'plus-o';

	// sampleExists && console.log('sample button', samplesForDepth[0])
	return (
		<button
			type="button"
			// onClick={onClick}
			onClick={
				() => onSampleCreateClick(locationId, depth.handle, sampleExists && samplesForDepth[0].id)
			}
			key={depth.handle}
			className={classes}
			disabled={!(hasGeolocation && isWithinRange || sampleExists)}
		>
			{depth.name}
			&nbsp;<Icon type={iconType} />
		</button>
	);
}