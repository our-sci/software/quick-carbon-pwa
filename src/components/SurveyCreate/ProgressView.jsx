import React, { useState, useContext } from 'react';
import PositionContext from 'contexts/PositionContext';
import SampleCreateModal from 'components/SampleCreate/SampleCreateModal';
import SampleButton from './SampleButton';

function getSamplesForLocation(locationId, sampleCollection) {
	if (locationId && sampleCollection) {
		return sampleCollection.filter((sample) => sample.properties.locationId === locationId);
	}
	return [];
}

function getLocation(id, locations) {
	return locations.features.find((l) => l.properties.id === id);
}

function getSample(id, samples) {
	if (!samples) {
		debugger;
	}
	return samples.find((s) => s.id === id);
}

export default function ProgressView({
	locations,
	samples,
	numberOfRequiredSamples,
	numberOfLocations,
	numberOfCompletedLocations,
	depths,
	onSampleCreate,
	surveyId,
	setSelectedLocationId,
	setShowReview
}) {
	const [positionState, setPositionState] = useContext(PositionContext);
	const [state, setState] = useState({
		sampleCreateDepthId: null,
		sampleCreateId: null,
		selectedLocationId: null,
	});
	const [modalIsVisible, setModalIsVisible] = useState(false);

	const location = state.selectedLocationId && getLocation(state.selectedLocationId, locations);


	function handleSampleCreateClick(locationId, depthId, id) {
		// console.log('sample create click')
		// const positionCopy = copyPosition(position);
		// console.log('handle sample create click', locationId, depthId, id);

		setState({
			...state,
			sampleCreateDepthId: depthId,
			sampleCreateId: id,
			selectedLocationId: locationId,
		});
		setModalIsVisible(true);
	}

	function handleSampleCreateClose() {
		setModalIsVisible(false);
	}

	function handleSampleCreateSubmit(sample) {
		onSampleCreate(sample);
		setModalIsVisible(false);
	}


	function getButtons(location, samples) {
		const locationSamples = getSamplesForLocation(location.id, samples);

		return depths.map((depth) => (
			<SampleButton
				depth={depth}
				isWithinRange={false}
				key={depth.handle}
				locationSamples={locationSamples}
				locationId={location.id}
				// onClick={() => onSampleCreate(location.id, depth.handle)}
				onSampleCreateClick={handleSampleCreateClick}
				hasGeolocation={false}
			/>
		));
	}

	function handleLocationClick(locationId) {
		setSelectedLocationId(locationId);
		setShowReview(false);
	}

	

	const list = locations.features.map((location) => (
		<tr key={location.id}>
			<td onClick={() => handleLocationClick(location.id)}>
				{location.properties.label}
			</td>
			<td>
				{getButtons(location, samples)}
			</td>

		</tr>
	));

	return (
		<div className="progress-view">
			<table className="ul-bare">
				<tbody>
					{list}
				</tbody>
			</table>
			{ location && (
				<SampleCreateModal
					onClose={handleSampleCreateClose}
					isVisible={modalIsVisible}
					location={location}
					depths={depths}
					surveyId={surveyId}
					isWithinValidRange={false}
					onSubmit={handleSampleCreateSubmit}
					depthId={state.sampleCreateDepthId}
					sampleId={!!state.sampleCreateId && getSample(state.sampleCreateId, samples).properties.sampleId}
					hiddenId={state.sampleCreateId}
					coordinates={state.sampleCreateId
						? getSample(state.sampleCreateId, samples).geometry.coordinates
						: !!positionState && [positionState.coords.longitude, positionState.coords.latitude]
					}
					timestamp={state.sampleCreateId
						? getSample(state.sampleCreateId, samples).properties.timestamp
						: !!positionState && positionState.timestamp
					}
				/>
			)}
		</div>
	)
}
