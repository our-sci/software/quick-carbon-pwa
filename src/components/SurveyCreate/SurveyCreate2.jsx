import React, { useContext, useState, useEffect } from 'react';
import Icon from '@ant-design/icons-react';
import { Link } from 'react-router-dom';
import AppContext from 'contexts/AppContext';
import MapboxGLMap from 'components/MBMap/MapboxGLMapSearchDraw';
import { watchNearestLocation } from 'utils/watchPosition';
import getNearestLocation from 'utils/getNearestLocation';
import { distance } from '@turf/turf';
import { point } from '@turf/helpers';
import copyPosition from 'utils/copyPosition';
import PositionContext, { PositionContextProvider } from 'contexts/PositionContext';

import LocationInfo from 'components/SurveyCreate/LocationInfo';
import EnableGeolocationMessage from 'components/SurveyCreate/EnableGeolocationMessage';
import SampleCreateModal from 'components/SampleCreate/SampleCreateModal';
import ObjectID from 'bson-objectid';

import exampleSampleCreateProps from 'data/exampleSampleCreateProps';
// window.ObjectID = ObjectID;

// // const ACCURACY_THRESHOLD = 1378;
// // const DISTANCE_THRESHOLD = 60;
// const ACCURACY_THRESHOLD = 15;
// const DISTANCE_THRESHOLD = 20;
const ACCURACY_THRESHOLD = process.env.REACT_APP_ACCURACY_THRESHOLD;
const DISTANCE_THRESHOLD = process.env.REACT_APP_DISTANCE_THRESHOLD;
window.ACCURACY_THRESHOLD = ACCURACY_THRESHOLD;
window.DISTANCE_THRESHOLD = DISTANCE_THRESHOLD;

const MAPBOX_TOKEN = process.env.REACT_APP_MAPBOX_TOKEN;

function Dummy({ setGeolocateListener }) {

	const [positionState, setPostionState] = useContext(PositionContext);
	

	return (
		<div>
			{/* pos:{pos && pos.coords && pos.coords.accuracy} */}
			pos:{positionState && positionState.coords && positionState.coords.accuracy}
		</div>
	);
}

function SurveyInstructions() {
	return (
		<div className="survey-instructions">
			<strong className="display-block">Instructions</strong>
			<p>
				Walk around to field locations and collect samples.
			</p>
		</div>
	)
}



export default function SurveyCreate({
	fieldId,
}) {
	const [appState, setAppState] = useContext(AppContext);
	// const [state, setState] = useState({ nearestLocation: null, position: null });
	const [state, setState] = useState({
		nearestLocation: null,
		selectedLocationId: null,
		surveyId: btoa(ObjectID().id),
		// sampleCreateModalIsVisible: false,
		sampleCreateDepthId: null,
		// sampleCreatePosition: null,
		sampleCreateId: null,
	});
	const [position, setPosition] = useState(null);
	const [modalIsVisible, setModalIsVisible] = useState(false);
	const [samples, setSamples] = useState([]);
	const [selectedLocationId, setSelectedLocationId] = useState(null);
	// const [selectedLocationId, setSelectedLocationId] = useState();
	// console.log('selectedLocationId', selectedLocationId)
	const [geolocateListener, setGeolocateListener] = useState([]);
	const [positionState, setPostionState] = useContext(PositionContext);


	const field = appState.fields.find((field) => String(field.id) === fieldId);
	// console.log('field', field);
	const depths = field.properties.depths.map(
		(depthId) => appState.soilDepths.find((
			depthObj) => depthId === depthObj.handle || depthId === depthObj.handle
		)
	).sort((a, b) => a.min_depth - b.min_depth);
	
	// console.log('depths', depths)
	const locations = appState.surveyLocations.find(
		(locations) => String(field.properties.surveyLocations) === String(locations.id),
	);
	function getLocations() {
		return appState.surveyLocations.find(
			(locations) => String(field.properties.surveyLocations) === String(locations.id),
		);
	}

	function getLocation(id) {
		return locations.features.find((l) => l.id === id);
	}

	function getSample(id) {
		return samples.find((s) => s.id === id);
	}

	function getCurrentLocation(sampleCreateId, selectedLocation, nearestLocation) {
		if (sampleCreateId) {
			const sample = getSample(sampleCreateId);
			const location = getLocation(sample.properties.locationId);
			return location;
		}
		return selectedLocation || nearestLocation;
		// return state.sampleCreateId
		// 	? getLocation(
		// 		getSample(state.sampleCreateId).properties.locationId,
		// 	)
		// 	: (selectedLocation || state.nearestLocation);
	}
	// console.log('locations', locations);

	// const popups = state.nearestLocation && position && {
	// 	type: 'FeatureCollection',
	// 	features: [
	// 		{
	// 			...state.nearestLocation,
	// 			properties: {
	// 				...state.nearestLocation.properties,
	// 				content: `
	// 					<div>
	// 						${(state.nearestLocation.properties.distanceToPoint * 1000).toFixed(2)}m
	// 						&nbsp;± ${position.coords.accuracy}m
	// 					</div>
	// 				`
	// 			},
	// 		}
	// 	],
	// };

	// const nearestIsWithinValidRange = state.nearestLocation
	// 	&& position
	// 	// && position.coords.accuracy < 151
	// 	&& position.coords.accuracy < 1378
	// 	// && state.nearestLocation.properties.distanceToPoint * 1e3 < 10;
	// 	&& state.nearestLocation.properties.distanceToPoint * 1e3 < 60;

	function getPointIsWithinValidRange(location, pos) {
		if (!location || !pos) {
			return null;
		}

		const distanceToPoint = distance(location.geometry, point([pos.coords.longitude, pos.coords.latitude]));
		return pos.coords.accuracy < ACCURACY_THRESHOLD
			&& distanceToPoint * 1e3 < DISTANCE_THRESHOLD;
	}

	// const nearestIsWithinValidRange = getPointIsWithinValidRange(state.nearestLocation, position);
	const nearestIsWithinValidRange = getPointIsWithinValidRange(state.nearestLocation, positionState);

	const showNearest = state.nearestLocation
		&& state.nearestLocation.properties.distanceToPoint * 1e3 < 60;

	// const hasGeolocation = !!position;
	// const hasNearestLocation = !!state.nearestLocation && !!position;
	const hasGeolocation = !!positionState;
	const hasNearestLocation = !!state.nearestLocation && !!positionState;

	// const selectedLocation = state.selectedLocationId
	// 	&& locations.features.find((marker) => marker.properties.id === state.selectedLocationId);

	const selectedLocation = selectedLocationId
		&& locations.features.find((marker) => marker.properties.id === selectedLocationId);
	// console.log('selected location', selectedLocation);
	// const selectedIsWithinValidRange = getPointIsWithinValidRange(selectedLocation, position);
	const selectedIsWithinValidRange = getPointIsWithinValidRange(selectedLocation, positionState);

	let activeLocationIsWithinRange = null;
	if (selectedLocation) {
		// activeLocationIsWithinRange = getPointIsWithinValidRange(selectedLocation, position);
		activeLocationIsWithinRange = getPointIsWithinValidRange(selectedLocation, positionState);
	} else if (state.nearestLocation) {
		// activeLocationIsWithinRange = getPointIsWithinValidRange(state.nearestLocation, position);
		activeLocationIsWithinRange = getPointIsWithinValidRange(state.nearestLocation, positionState);
	}

	function getSamplesForLocation(locationId, sampleCollection) {
		if (locationId && sampleCollection) {
			return sampleCollection.filter((sample) => sample.properties.locationId === locationId);
		}
		return [];
	}
	let samplesForActiveLocation = [];
	if (selectedLocation) {
		// samplesForActiveLocation = getSamplesForLocation(state.selectedLocationId, samples);
		samplesForActiveLocation = getSamplesForLocation(selectedLocationId, samples);
	} else if (state.nearestLocation) {
		samplesForActiveLocation = getSamplesForLocation(state.nearestLocation.id, samples);
	}
	// console.log(samplesForActiveLocation);

	const distanceToSelectedLocation = hasGeolocation && selectedLocation
		// && distance(selectedLocation.geometry, point([position.coords.longitude, position.coords.latitude]));
		&& distance(selectedLocation.geometry, point([positionState.coords.longitude, positionState.coords.latitude]));
		// console.log(distanceToSelectedLocation);

	// function handleWatchSuccess(nearPoint, distanceToPoint, currentLocation, pos) {
	// 	console.log(
	// 		'nearpoint', nearPoint,
	// 		'distancetoPoint', distanceToPoint,
	// 		'currlocation', currentLocation,
	// 		'pos', pos,
	// 	);
	// }

	// watchNearestLocation(locations, handleWatchSuccess);
	// window.watch = () => watchNearestLocation(locations, handleWatchSuccess);

	function handleSampleCreateClick(locationId, depthId, id) {

		// const positionCopy = copyPosition(position);
		console.log('hanlde sample create click', locationId, depthId, id);

		setState({
			...state,
			// sampleCreateModalIsVisible: true,
			sampleCreateDepthId: depthId,
			// sampleCreatePosition: positionCopy,
			sampleCreateId: id,
		});
		setModalIsVisible(true);
	}
	
	function handleSampleCreateClose() {		
		setState({
			...state,
			// sampleCreateModalIsVisible: false,
			sampleCreateDepthId: null,
			// sampleCreatePosition: null,
			sampleCreateId: null,
		});
		setModalIsVisible(false);
	}

	function handleSampleCreate(sample) {
		// TODO: merge in sample, prevent duplicates by checking ids
		const sampleExistsIndex = samples.findIndex((s) => s.id === sample.id);
		let nextSamples;
		if (sampleExistsIndex > -1) {
			nextSamples = [
				...samples.slice(0, sampleExistsIndex),
				...samples.slice(sampleExistsIndex, samples.length),
				sample,
			];
		} else {
			nextSamples = [
				...samples,
				sample,
			];
		}

		setState({ 
			...state,
			sampleCreateModalIsVisible: false,
			sampleCreateDepthId: null,
		});
		setModalIsVisible(false);
		setSamples(nextSamples);
	}

	function handleMapClick(ev) {
		// console.log(ev);
		// ev.target.featuresAt(ev.point, { layer: 'locations' }, (err, features) => {
		// 	console.log(features);
		// })
		const features = ev.target.queryRenderedFeatures(ev.point);
		if (features && features[0]) {
			// if (features[0].properties.id !== state.selectedLocationId) {
			if (features[0].properties.id !== selectedLocationId) {
				setState({ ...state, selectedLocationId: features[0].properties.id });
				setSelectedLocationId(features[0].properties.id );
			} else {
				setState({ ...state, selectedLocationId: null });
				setSelectedLocationId(null);
			}

		}
	}

	// let dummyPos = {};
	const handleGeolocate = (pos) => {
		console.log('survey create handle geolocate', pos);
		// setTimeout(() => setGeolocateListener(), 1000);
		
		// geolocateListener && geolocateListener(pos);

		// const nearestLocation = getNearestLocation(pos, locations);

		// setState({ ...state, nearestLocation: {
		// 	...nearestLocation,
		// 	properties: {
		// 		...nearestLocation.properties,
		// 		type: "NEAREST_LOCATION_MARKER",
		// 	},
		// }});
		// setPosition(pos);
	}

	// Set customMarker to highlight the currently selected location if set by user
	// Otherwise customMarker will highlight the nearest location, if present
	let customMarkers;
	if (selectedLocation) {
		customMarkers = {
			type: 'FeatureCollection',
			features: [selectedLocation],
		}
	} else if (showNearest && state.nearestLocation) {
		customMarkers = {
			type: 'FeatureCollection',
			features: [state.nearestLocation],
		};
	}

	// const customlocations = state.nearestLocation && {
	// 	type: 'FeatureCollection',
	// 	features: [state.nearestLocation],
	// };

	// const customMarkers = selectedLocation && {
	// 	type: 'FeatureCollection',
	// 	features: [selectedLocation],
	// };

	return (
			<div className="survey-create">
				<Dummy setGeolocateListener={setGeolocateListener} />
				<header className="page-header text-align-center position-relative">
					<Link to="/field" className="position-absolute left-10 vertical-center-top-transform">
						<button type="button" className="button icon-only circle no-bg">
							<Icon type="arrow-left-o" />
						</button>
					</Link>
					<h1 className="page-title">Survey Create</h1>
					<div className="page-subtitle"></div>
				</header>
				<main>
					<div className="position-relative">

						{(
							<MapboxGLMap
								features={{ type: 'FeatureCollection', features: [field] }}
								markers={locations}
								customMarkers={customMarkers}
								// popups={popups}
								height="calc(100vh - 300px)"
								width="100%"
								hasGeolocateControl
								geolocateCallback={geolocateListener}
								eventListeners={[
									{
										type: 'click',
										listener: handleMapClick,
										layerId: 'markers',
									},
									
								]}
								
							/>
						)}
						{!hasGeolocation && (
							<EnableGeolocationMessage
								className="position-absolute bottom-0"
							/>
						)}
					</div>
					<section className="bottom padding-10">
						{/* {!hasGeolocation && !state.selectedLocationId && (<SurveyInstructions />)} */}
						{!hasGeolocation && !selectedLocationId && (<SurveyInstructions />)}
						{/* {hasGeolocation && !state.selectedLocationId && !nearestIsWithinValidRange && ( */}
						{hasGeolocation && !selectedLocationId && !nearestIsWithinValidRange && (
							<div>
								You are out of range of any sampling locations.
							</div>
						)}
						{hasNearestLocation && !selectedLocation && showNearest && (
							<LocationInfo
								location={state.nearestLocation}
								distance={state.nearestLocation.properties.distanceToPoint}
								depths={depths}
								isWithinRange={nearestIsWithinValidRange}
								// accuracy={position.coords.accuracy}
								accuracy={positionState.coords.accuracy}
								onSampleCreate={handleSampleCreateClick}
								locationSamples={samplesForActiveLocation}
								type="AUTO"
							/>
						)}
						{selectedLocation && (
							<LocationInfo
								// location={state.nearestLocation}
								// distance={state.nearestLocation.properties.distanceToPoint}
								location={selectedLocation}
								// distance={'5'}
								distance={distanceToSelectedLocation}
								depths={depths}
								isWithinRange={selectedIsWithinValidRange}
								// accuracy={hasGeolocation && position.coords.accuracy}
								accuracy={hasGeolocation && positionState.coords.accuracy}
								onSampleCreate={handleSampleCreateClick}
								locationSamples={samplesForActiveLocation}
								type="SELECTED"
							/>
						)}
					</section>
				</main>
				{state.sampleCreateId && console.log(getSample(state.sampleCreateId))}
				<SampleCreateModal 
					onClose={handleSampleCreateClose}
					// isVisible={state.sampleCreateModalIsVisible}
					isVisible={modalIsVisible}
					// location={state.sampleId 
					location={getCurrentLocation(state.sampleCreateId, selectedLocation, state.nearestLocation)}
					depths={depths}
					surveyId={state.surveyId}
					isWithinValidRange={activeLocationIsWithinRange}
					onSubmit={handleSampleCreate}
					depthId={state.sampleCreateDepthId}
					sampleId={!!state.sampleCreateId && getSample(state.sampleCreateId).properties.sampleId}
					hiddenId={state.sampleCreateId}
					// position={position}
					// position={state.sampleCreatePosition}
					coordinates={state.sampleCreateId
						? getSample(state.sampleCreateId).geometry.coordinates
						// : !!position && [position.coords.longitude, position.coords.latitude]
						: !!positionState && [positionState.coords.longitude, positionState.coords.latitude]
					}
					timestamp={state.sampleCreateId
						? getSample(state.sampleCreateId).properties.timestamp
						// : !!position && position.timestamp
						: !!positionState && positionState.timestamp
					}
				/>
				{/* <SampleCreateModal 
					onClose={handleSampleCreateClose}
					// isVisible={state.sampleCreateModalIsVisible}
					isVisible={modalIsVisible}
					location={exampleSampleCreateProps.location}
					depths={exampleSampleCreateProps.depths}
					position={exampleSampleCreateProps.position}
					surveyId={exampleSampleCreateProps.surveyId}
					isWithinValidRange={true}
					onSubmit={handleSampleCreate}
				/> */}
				{/* <Modal 
					onClose={handleSampleCreateClose}
					isVisible={state.sampleCreateModalIsVisible}
				>
					hey
				</Modal> */}
			</div>
	)	
}
