import React, { useContext, useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch, Route/*, Link */ } from 'react-router-dom';

import FieldCreatePage from 'pages/field-create';
import FieldIndexPage from 'pages/field/index';
import FieldShowPage from 'pages/field/show';
// import SamplingEventIndexPage from 'pages/sampling-event/index';
// import SamplingEventCreatePage from 'pages/sampling-event/create';
import SurveyCreatePage from 'pages/survey-create';
import SurveyCreatePage3 from 'pages/survey-create3';
import SurveyShow from 'pages/survey/show';
import IntroPage from 'pages/intro.jsx';
import SetupModal from 'components/SetupModal/SetupModal';
import addIcons from 'utils/addIcons';


// import Modal from 'components/Modal/Modal';

import { AppContextProvider } from 'contexts/AppContext';
import { PositionContextProvider } from 'contexts/PositionContext';
import './App.css';

import AppContext from 'contexts/AppContext';


// import { openDB } from 'idb';
// import { fields } from 'data/db';
// async function databaseStuff() {
// 	const db = await openDB('keyval-store', 0, {
// 		upgrade(db) {
// 			const store = db.createObjectStore('fields', {
// 				keyPath: 'id',
// 				autoIncrement: true,
// 			});
// 			store.createIndex('id', 'id');
// 		}
// 	});
// }
// async function dbStuff2() {
// 	const tx = db.transaction('fields', 'readwrite');
// 	fields.forEach((field) => {
// 		tx.store.add({ ...field });
// 	});
// 	await tx.done;
// }

// window.databaseStuff = databaseStuff;
// window.dbStuff2 = dbStuff2;

// const dbPromise = openDB('keyval-store', 1, {
// 	upgrade(db) {
// 		db.createObjectStore('keyval');
// 	}
// });

// const idbKeyval = {
// 	async get(key) {
// 		return (await dbPromise).get('keyval', key);
// 	},
// 	async set(key, val) {
// 		return (await dbPromise).put('keyval', val, key);
// 	},
// 	async delete(key) {
// 		return (await dbPromise).delete('keyval', key);
// 	},
// 	async clear() {
// 		return (await dbPromise).clear('keyval');
// 	},
// 	async keys() {
// 		return (await dbPromise).getAllKeys('keyval');
// 	},
// };


addIcons();

const BACKEND_BASE_URL = process.env.REACT_APP_BACKEND_URL;


function AppContent() {

	const [appState, setAppState] = useContext(AppContext);
	useEffect(() => {
		async function fetchData() {
			let fetchedFields = [];
			let fetchedLocationCollections = [];
			let fetchedSurveys = [];
			try {
				const fieldResponse = await window.fetch(`${BACKEND_BASE_URL}/field`);
				fetchedFields = await fieldResponse.json();
				console.log('fetched fields', fetchedFields);
				// setAppState({ ...appState, fields: fieldBody });
			} catch (err) {
				console.log(err);
			}

			try {
				const locationCollectionReponse = await window.fetch(`${BACKEND_BASE_URL}/locationcollection`);
				fetchedLocationCollections = await locationCollectionReponse.json();
				// debugger;
				console.log('fetched location collection', fetchedLocationCollections);
				// setAppState({ ...appState, fields: fieldBody });
			} catch (err) {
				console.log(err);
			}

			try {
				const surveyResponse = await window.fetch(`${BACKEND_BASE_URL}/samplingevent`);
				fetchedSurveys = await surveyResponse.json();
				// debugger;
				console.log('fetched surveys/samplingevents', fetchedSurveys);
				// setAppState({ ...appState, fields: fieldBody });
			} catch (err) {
				console.log(err);
			}

			setAppState({
				...appState,
				fields: fetchedFields,
				surveyLocations: fetchedLocationCollections,
				samplingEvents: fetchedSurveys,
			});


		}

		fetchData();
	}, []);

	return (
		<div className="App">
			<Router>
				<header>
					{/* <ul>
								<li><Link to="/">Intro</Link></li>
								<li><Link to="/field">Fields</Link></li>
								<li><Link to="/field/create">Field Create</Link></li>
								<li><Link to="/sampling-event">Sampling Events</Link></li>
								<li><Link to="/sampling-event/create">Sampling Event Create</Link></li>
							</ul> */}
				</header>
				<main>
					<Switch>
						<Route path="/" exact component={IntroPage} />
						<Route path="/field/" exact component={FieldIndexPage} />
						<Route path="/field/:id/survey-create" component={SurveyCreatePage} />
						<Route path="/field/:id/survey-create3" component={SurveyCreatePage3} />
						<Route path="/field/:id" exact component={FieldShowPage} />
						<Route path="/survey/:id" component={SurveyShow} />
						{/* <Route path="/survey-create" component={SurveyCreatePage} /> */}
						<Route path="/field-create" component={FieldCreatePage} />
						{/* <Route path="/sampling-event/" exact component={SamplingEventIndexPage} /> */}
						{/* <Route path="/sampling-event/create" component={SamplingEventCreatePage} /> */}
					</Switch>
				</main>
				{/* <button type="button" onClick={() => setIntroIsVisible(true)}>Open Intro</button> */}
				{/* <Modal isVisible={introIsVisible} handleClose={() => setIntroIsVisible(false)}>
							<div>Some content</div>
						</Modal> */}
				<SetupModal />
			</Router>
		</div>
	);
}

function App() {
	// const [introIsVisible, setIntroIsVisible] = useState(false);
	
	
	return (
		<AppContextProvider>
			<PositionContextProvider>
				<AppContent />
				
			</PositionContextProvider>
		</AppContextProvider>
	);
}

export default App;
