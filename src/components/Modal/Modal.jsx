import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import styles from './Modal.module.css';

/**
 * Could change this component to not handle isVisible, let parent decide whether to render 
 * or not. This way we could use ReactCSSTransitionGroup to handle transitions
 */
export default function Modal({ 
	onClose,
	isVisible,
	className,
	options,
	children,
	height,
	width, 
	verticalCenter,
	horizontalCenter,
}) {
	const modalInnerEl = useRef(null);

	useEffect(() => {
		const node = modalInnerEl.current;
		function handleOutsideClick(ev) {
			if (node.contains(ev.target) && !node.isSameNode(ev.target)) {
				return;
			}
			onClose();
		}
		isVisible && node.addEventListener('click', handleOutsideClick);
		return () => isVisible && node.removeEventListener('click', handleOutsideClick);
	}, [onClose, isVisible]);

	useEffect(() => {
		function handleKeyUp(ev) {
			if (ev.keyCode === 27 || ev.key === 'Escape') {
				onClose();
			}
		}

		window.addEventListener('keyup', handleKeyUp, false);
		return () => window.removeEventListener('keyup', handleKeyUp);
	}, [onClose]);

	const classes = classNames(
		styles.modal,
		{
			'display-block': isVisible,
			'display-flex': isVisible,
			'display-none': !isVisible,
			[styles['vertical-center']]: verticalCenter,
			[styles['horizontal-center']]: horizontalCenter,

		},
		className,
	);

	return isVisible && (
		<div 
			className={classes} 
			role="dialog"
			ref={modalInnerEl}
		>
			<div 
				className={styles['modal-inner']}
				style={{ height, width }}
			>
				<header>
					{options.showClose && (
						<button
							type="button"
							className={styles['close-button']}
							onClick={onClose}
						>
							&times;
						</button>
					)}
				</header>
				<div className={styles['modal-body']}>
					{children}
				</div>
			</div>
		</div>
	);
}

Modal.propTypes = {
	onClose: PropTypes.func,
	isVisible: PropTypes.bool.isRequired,
	className: PropTypes.string,
	options: PropTypes.shape({
		showClose: PropTypes.bool,
	}),
	height: PropTypes.string,
	width: PropTypes.string,
	verticalCenter: PropTypes.bool,
	horizontalCenter: PropTypes.bool,
};

Modal.defaultProps = {
	onClose: () => {},
	className: '',
	options: {
		showClose: true,
	},
	height: 'auto',
	width: '85%',
	verticalCenter: true,
	horizontalCenter: true,
};
