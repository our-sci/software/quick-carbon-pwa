import React, { useState } from 'react';

import Modal from 'components/Modal/Modal';
import classNames from 'classnames';
import { markersSchema } from 'components/FieldCreate/schemas';
// import { feature } from '@turf/turf';
// import { csv2geojson } from 'csv2geojson';
import csvToGeoJSON from 'utils/csvToGeoJSON';
import ObjectID from 'bson-objectid';


export default function MarkersFileUploadModal({
	isVisible,
	onSubmit,
	onClose,
}) {
	const [state, setState] = useState({
		file: null,
		touched: null,
		errors: null,
		featureCollection: null,
	});

	async function handleChange(event) {
		const file = event.target.files[0];
		// event.target
		console.log(event.target.files[0]);
		setState({ ...state, isDirty: true });
		if (/\.csv$/.test(file.name) /*&& file.type === 'text/csv'*/) {
			console.log(file);
			const csvString = await file.text();
			// console.log(csvString.split('\n'));
			// const featureCollection = {};
			
			// csv2geojson(csvString, (err, data) => {
			// 	console.log(err, data);
			// });
			const featureCollection = await csvToGeoJSON(csvString);
			// console.log(featureCollection);
			
			// // Add id to each feature if missing
			if (featureCollection.features.every((feature) => !Object.keys(feature).includes('id'))) {
				featureCollection.features.forEach((feature, index) => {
					
					// feature.id = index;
					// feature.properties.label = index;
					const id = ObjectID().str;
					feature.id = id;
					feature.properties.id = id;
					feature.properties.label = index;
				});
			}

			// featureCollection.features.forEach((feature, index) => {
			// 	const id = ObjectID().str;
			// 	feature.id = id;
			// 	feature.properties.id = id; 
			// 	feature.properties.label = String(index);
			// 	// feature.properties.type = 'SURVEY_LOCATION_MARKER';
			// });

			// featureCollection.id = ObjectID().str;

			try {
				markersSchema.validateSync(featureCollection.features)
			} catch (error) {
				console.log(error);
			}
			const markersIsValid = markersSchema.isValidSync(featureCollection.features);
			// !featuresIsValid && console.log('file features is not valid');
			// featuresIsValid && console.log('file features is valid');
			if (markersIsValid) {
				setState({ ...state, file, featureCollection, touched: true, errors: null });
			} else {
				setState({ ...state, errors: ['File features are invalid'], touched: true });
			}
		} else {
			setState({ ...state, errors: ['Unsupported file type'], touched: true });
		}
	}

	// function handleSubmit() {
	// 	onClick(state.featureCollection);
	// }

	// const fieldGroupClasses = classNames()
	const inputClasses = classNames(
		['input', 'file', 'padding-10', 'border-radius-4'],
		{
			error: state.errors,
		},
	);

	return (
		<Modal 
			isVisible={isVisible}
			onClose={onClose}
			height="auto"
			width="85%"
		>
			<div className="field-group position-relative margin-top-10">
				<label className="label" htmlFor="markers-file">Survey Locations File</label>
				<input
					type="file"
					name="markers-file"
					id="markers-file"
					className={inputClasses}
					onChange={handleChange}
				/>
				{state.errors && <div className="message error">{state.errors}</div>}
			</div>
			<div className="text-align-center margin-top-25">
				<button
					type="button"
					className="button primary"
					onClick={() => onSubmit(state.featureCollection)}
					disabled={state.errors || !state.touched}
				>
					Import
				</button>
			</div>
		</Modal>
	);
}
