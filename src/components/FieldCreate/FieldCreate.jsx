import React, { useState, useContext } from 'react';
import AppContext from 'contexts/AppContext';
import FieldDraw from './FieldDraw';
// import FieldPropertiesForm from './FieldPropertiesForm';
import FormikFieldForm from './FormikFieldForm';
import FieldLocations from './FieldLocations';
import { Switch, Route, Link } from 'react-router-dom';
import ObjectID from 'bson-objectid';
import submitToOurSci from 'utils/submitToOurSci';


// import exampleField from 'data/exampleField';
import { fields } from 'data/db';

const BACKEND_BASE_URL = process.env.REACT_APP_BACKEND_URL;

async function postData(url = '', data = {}) {
	const response = await fetch(url, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(data),
	});
	console.log(response);
	return await response.json();
}

export default function FieldCreate() {
	const [appState, setAppState] = useContext(AppContext);

	const [fieldState, setFieldState]	= useState({
		// stage: 0,
		field: {
			type: 'Feature',
			geometry: {},
			id: null,
			properties: {
				handle: '',
				organization: '', // need to pull this from context
				name: '',
				depths: [],
				hasAdditionalQuestions: false,
				email: '',
				createdAt: null,
			},
		},
		// field: exampleField,
		// field: fields[1],
	});

	function setField(field) {
		setFieldState({ ...fieldState, field });
	}

	function onNextPropertiesForm(properties, history) {
		setFieldState({
			...fieldState,
			field: { ...fieldState.field, properties },
		});
		history.push('/field-create/locations');
	}

	function onNextFieldDraw(feature, history) {
		setFieldState({ ...fieldState, field: feature });
		history.push('/field-create/properties');
	}

	async function onNextLocations(locations, history) {
		console.log('locations', locations);
		const locationsId = ObjectID().str;
		setFieldState({ ...fieldState, locations });

		const newField = {
			...fieldState.field,
			_id: fieldState.field.id,
			properties: {
				...fieldState.field.properties,
				surveyLocations: locationsId,
				samplingEvents: [],
			},
		};

		const newLocationCollection = {
			id: locationsId,
			...locations,
		};
		// const newLocationCollection2 = {
		// 	_id: locationsId,
		// 	locations: locations.map((location) => location.id),
		// };

		setAppState({ 
			...appState,
			fields: [
				...appState.fields,
				newField,
			],
			surveyLocations: [
				...appState.surveyLocations,
				newLocationCollection,
			],
		});

		try {
			// console.log('new field', JSON.stringify(newField));
			const response = await postData(`${BACKEND_BASE_URL}/field`, newField);
			console.log(response);
		} catch (err) {
			console.log(err);
			debugger;
		}

		try {
			// TODO: create array of Locations /location and LocationCollection with their IDs (/locationcollection)
			console.log('new location collection', JSON.stringify(newLocationCollection));
			const response = await postData(`${BACKEND_BASE_URL}/locationcollection`, newLocationCollection);
			console.log(response);
		} catch (err) {
			console.log(err);
			debugger;
		}

		try {
			const response = await submitToOurSci( 
				{ field: newField },
				{ id: newField.id, createdAt: Date.now() },
				'field',
			);
			console.log('oursci field create response', response);
		} catch (err) {
			console.log(err);
		}

		history.push('/field/');
	}

	return (
		<div className="field-create">
			{/* {getStage(fieldState.stage)} */}
			<Switch>
				<Route 
					path="/field-create"
					exact 
					component={({ history }) => (
						<FieldDraw 
							field={fieldState.field}
							setField={setField}
							onNext={(feature) => onNextFieldDraw(feature, history)}
						/>
					)}
				/>
				<Route
					path="/field-create/properties"
					component={({ history }) => (
						<FormikFieldForm
							onBack={history.goBack}
							onNext={(properties) => onNextPropertiesForm(properties, history)}
							properties={fieldState.field.properties}
						/>
					)}
				/>
				<Route
					path="/field-create/locations"
					component={({ history}) => (
						<FieldLocations
							onBack={history.goBack}
							field={fieldState.field}
							onNext={(locations) => onNextLocations(locations, history)}
						/>
					)}
				/>
			</Switch>
		</div>
	);
}
