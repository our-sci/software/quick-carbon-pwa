import React from 'react';
import { soilDepths } from 'data/db';


function Checkbox({ label, type = 'checkbox', handle, checked = false, onChange }) {
	return (
		<div>
			<input type={type} name={handle} checked={checked} id={handle} onChange={onChange} />
			<label htmlFor={handle}>{label}</label>
		</div>
	);
}

export default function FieldPropertiesForm({ fieldState, setFieldState }) {
	// const [formState, setFormState] = useState()

	const checkboxes = soilDepths.map((sd) => {
		return (
			<Checkbox
				label={sd.name}
				handle={sd.handle}
				key={sd.handle}
			/>
		);
	});

	function handleInputChange(ev) {
		const { target } = ev;
		const value = target.type === 'checkbox' ? target.checked : target.value;
		const { name } = target;
		setFieldState({
			...fieldState,
			properties: {
				...fieldState.properties,
				[name]: value,
			},
		});
	}

	return (
		<form>
			<div>
				<label htmlFor="handle">Field Handle</label>
				<input
					type="text"
					name="handle"
					id="handle"
					value={fieldState.properties.handle}
					onChange={handleInputChange}
				/>
			</div>
			<div>
				<label htmlFor="organization">Organization</label>
				<input
					type="text"
					name="organization"
					id="organization"
					value={fieldState.properties.organization}
					onChange={handleInputChange}
				/>
			</div>
			<div>
				<label htmlFor="name">Field Name</label>
				<input
					type="text"
					name="name"
					id="name"
					value={fieldState.properties.name}
					onChange={handleInputChange}
				/>
			</div>
			<div>
				<label htmlFor="email">Owner Email</label>
				<input
					type="email"
					name="email"
					id="email"
					value={fieldState.properties.email}
					onChange={handleInputChange}
				/>
			</div>
			<div>
				<label htmlFor="soil-depth">Soil Depths</label>
				{/* <input type="checkbox" name="soil-depth" value={fieldState.properties.soilDepths} /> */}
				{checkboxes}
			</div>
			<div>
				<label htmlFor="soil-depth">Soil Depths</label>
				{/* <input type="checkbox" name="soil-depth" value={fieldState.properties.soilDepths} /> */}
				{checkboxes}
			</div>

		</form>
	);
}

