import React, { useContext } from 'react';
import {
	Formik,
	Form,
	Field,
	ErrorMessage,
} from 'formik';
import classNames from 'classnames';
import * as Yup from 'yup';
// import { Link } from 'react-router-dom';
import handleize from 'utils/handleize';
import AppContext from 'contexts/AppContext';
import { soilDepths, organizations } from 'data/db';
import Icon from '@ant-design/icons-react';
import 'components/Form/form.css';

import Checkbox from 'components/Form/Checkbox';
import CheckboxGroup from 'components/Form/CheckboxGroup';
import Select from 'components/Form/Select';
import InputField from 'components/Form/InputField';


// const soilDepthsCheckboxesData = soilDepths.map(sd => ({
// 	id: sd.handle,
// 	name: sd.handle,
// 	label: sd.name,
// }));

// const formFields = [
// 	{
// 		label: 'Field Handle',
// 		type: 'text',
// 		disabled: true,
// 	},
// 	{
// 		label: 'Organization',
// 		type: 'text',
// 		disabled: true,
// 	},
// 	{
// 		label: 'Field Name',
// 		type: 'text',
// 	},
// 	{
// 		label: 'Owner\'s Email',
// 		type: 'email',
// 	},
// 	{
// 		label: 'Depths to Measure',
// 		type: 'checkbox-group',
// 		source: soilDepthsCheckboxesData,
// 	},
// 	{
// 		label: 'Ask Hand Texture Questions',
// 		type: 'checkbox',
// 	},
// ];




export default function FormikFieldForm({
	onNext, 
	onBack,
	properties,
}) {
	// appContextValue = useContext(App)
	const [appState, setAppState] = useContext(AppContext);

	const initialValues = {
		handle: '',
		organization: appState.organization, // need to pull this from context
		name: '',
		depths: [],
		hasAdditionalQuestions: false,
		email: '',
		...properties,
	};

	// function validate(values) {
	// 	let errors = {};
	// 	if (!values.email) {
	// 		errors.email = 'Required';
	// 	} else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
	// 		errors.email = 'Invalid email address';
	// 	}
	// 	return errors;
	// }

	function handleSubmit(values, { setSubmitting }) {
		// console.log(values);
		// setTimeout(() => {
		// 	console.log(JSON.stringify(values, null, 2));
		// 	setSubmitting(false);
		// }, 1);
		// setAppState({ ...appState, values})
		onNext({
			...values,
			handle: handleize(values.name),
			createdAt: Date.now(),
		});
	}

	// function validate(values) {
	// 	let errors = {};

	// 	if (values.name) {
	// 		validate uniqueness of handleized name
	// 	}
	// }

	// TODO: need to add validation for handle collisions
	// not sure how handles will actually be used though so skipping for now
	const validationSchema = Yup.object().shape({
		// handle: Yup.string().required()
		name: Yup.string().min(4, 'please enter at least 4 characters')
			.matches(/^[A-z\d\-_\s]+$/, {
				message: 'special characters are not allowed',
			}).required('a field name is required'),
		depths: Yup.array().min(1, 'at least one depth must be selected')
			.required('Soil Depth is required'),
		hasAdditionalQuestions: Yup.boolean(),
		email: Yup.string().email('please enter a valid email')
			.required('a valid email is required')
	});

	const soilDepthCheckboxes = soilDepths.map((sd) => (
		<Field
			component={Checkbox}
			name="depths"
			id={sd.handle}
			label={sd.name}
			key={sd.handle}
		/>
	));

	return (
		<div className="FormikFieldForm form-container">
			<header className="page-header text-align-center position-relative">
				{/* <Link to="/field" className="position-absolute left-10 vertical-center-top-transform">
					<button type="button" className="button icon circle no-bg">
						<Icon type="arrow-left-o" />
					</button>
					
				</Link> */}
				<div className="position-absolute left-0 vertical-center-top-transform">

					<button type="button" className="button circle icon no-bg" onClick={onBack} /*disabled={isSubmitting}*/>
						<Icon type="arrow-left-o" />
						{/* Back */}
					</button>
				</div>
				<div>
					<h2 className="page-title">Field Create</h2>
					<div className="page-subtitle">Enter Field Attributes</div>
				</div>
			</header>
			<Formik
				initialValues={initialValues}
				// validate={validate}
				validationSchema={validationSchema}
				onSubmit={handleSubmit}
			>
				{({
					isSubmitting,
					// handleSubmit,
					values,
					errors,
					touched,
					setFieldValue,
					setFieldTouched,
				}) => {
					return (
						<Form className="padding-0-25">
							
							<Field
								component={InputField}
								type="text"
								name="name"
								id="name"
								label="Field Name"
							/>
							<Field
								component={InputField}
								type="email"
								name="email"
								id="email"
								label="Owner Email"
							/>

							{/* <Field
								component={InputField}
								type="text"
								name="fieldHandle"
								id="fieldHandle"
								label="Field Handle"
								fieldValue={handleize(values.name)}
								disabled
							/> */}
							{/* Couldn't figure out how to get a computed value to work with InputField */}
							<div className="field-group">
								<label className="label display-block" htmlFor="fieldHandle">Field Handle</label>
								<input
									type="text"
									name="fieldHandle"
									id="fieldHandle"
									value={handleize(values.name)}
									className="input text width-100"
									disabled
								/>
								<ErrorMessage className="error message input-message" name="fieldHandle" component="div" />
							</div>
							<Field
								component={Select}
								name="organization"
								id="organization"
								label="Organization"
								disabled
							>
								{organizations.map((org) => (
									<option value={org.id} key={org.id}>{org.name}</option>
								))}
							</Field>

							<CheckboxGroup
								id="depths"
								label="Soil Depths"
								value={values.depths}
								error={errors.depths}
								touched={touched.depths}
								onChange={setFieldValue}
								onBlur={setFieldTouched}
							>
								{soilDepths.map((sd) => (
									<Field
										component={Checkbox}
										name="depths"
										id={sd.handle}
										label={sd.name}
										key={sd.handle}
									/>
								))}
							</CheckboxGroup>
							<Field
								component={Checkbox}
								className="field-group"
								name="hasAdditionalQuestions"
								id="hasAdditionalQuestions"
								label="Ask Hand Texturing Questions"
							/>
							
							<div className="padding-25-0 text-align-center">
								
								<button type="submit" className="button primary " disabled={isSubmitting}>
									Continue
								</button>
							</div>
						</Form>
					);
				}}
			</Formik>
		</div>
	);
}
