import React, { useState } from 'react';
import Modal from 'components/Modal/Modal';
import { kml } from '@tmcw/togeojson';
// import { bbox } from '@turf/turf';
import classNames from 'classnames';
import { featureCollection } from '@turf/turf';
import ObjectID from 'bson-objectid';
import { fieldFeaturesSchema } from './schemas';

export default function FileUploadModal({
	isVisible,
	onClose,
	setField,
}) {
	const [state, setState] = useState({ file: null, touched: null });
	async function handleChange(event) {
		const file = event.target.files[0];
		// event.target
		console.log(event.target.files[0]);
		setState({ ...state, isDirty: true });
		if (/\.kml$/.test(file.name)) {
			const xmlString = await file.text();
			const dom = new DOMParser().parseFromString(xmlString, 'text/xml');
			const featureCollection = kml(dom);
			// console.log(featureCollection);
			// Add id to each feature if missing
			// if (featureCollection.features.every((feature) => !Object.keys(feature).includes('id'))) {
			// 	featureCollection.features.forEach((feature, index) => {
			// 		feature.id = `${file.name}.${index}`;
			// 		feature.properties.label = index + 1;
			// 	});
			// }
			featureCollection.features.forEach((feature, index) => {
				// feature.id = `${file.name}.${index}`;
				feature.id = ObjectID().str;
				feature.properties.label = index + 1;
			});


			try {
				fieldFeaturesSchema.validateSync(state.featureCollection.features);
			} catch (error) {
				console.log(error);
			}
			const featuresIsValid = fieldFeaturesSchema.isValidSync(featureCollection.features);
			// !featuresIsValid && console.log('file features is not valid');
			// featuresIsValid && console.log('file features is valid');
			if (featuresIsValid) {
				setState({
					...state, file, featureCollection, touched: true, errors: null,
				});
			} else {
				setState({ ...state, errors: ['File features are invalid'], touched: true });
			}
		} else if (/\.geojson$/.test(file.name)) {
			const jsonString = await file.text();
			const featureCollection = JSON.parse(jsonString);
			const featuresIsValid = fieldFeaturesSchema.isValidSync(featureCollection.features);

			// const validationResults = fieldFeaturesSchema.validateSync(featureCollection.features);
			// console.log(validationResults);
			// debugger;

			if (featuresIsValid) {
				setState({
					...state, file, featureCollection, touched: true, errors: null,
				});
			} else {
				setState({ ...state, errors: ['File features are invalid'], touched: true });
			}
		} else {
			setState({ ...state, errors: ['Unsupported file type'], touched: true });
		}
	}

	function handleSubmit() {
		// console.log(state.featureCollection.features);
		setField(state.featureCollection.features[0]);
		onClose();
	}

	// const fieldGroupClasses = classNames()
	const inputClasses = classNames(
		['input', 'file', 'padding-10', 'border-radius-4'],
		{
			error: state.errors,
		},
	);
	return (
		<Modal
			isVisible={isVisible}
			onClose={onClose}
			height="auto"
			width="85%"
		>
			<div className="">

				<div className="field-group position-relative margin-top-10">
					<label className="label display-block" htmlFor="feature-file">Field Polygon File</label>
					<input
						type="file"
						name="feature-file"
						id="feature-file"
						className={inputClasses}
						onChange={handleChange}
					/>
					{state.errors && <div className="message error">{state.errors}</div>}
				</div>
				<div className="text-align-center margin-top-10">
					<button
						type="button"
						className="button primary"
						onClick={handleSubmit}
						disabled={state.errors || !state.touched}
					>
						Add to Map
					</button>
				</div>
			</div>
		</Modal>
	);
}
