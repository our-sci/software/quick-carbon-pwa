import React, { useState } from 'react';
import MapboxGLMap from 'components/MBMap/MapboxGLMapSearchDraw';
import MarkersFileUploadModal from 'components/FieldCreate/MarkersFileUploadModal';
import randomPointsInPoly from 'utils/randomPointsInPolygon';
import download from 'utils/download';
import copyToClipboard from 'utils/copyToClipboard';
import { fieldFeatureSchema, markersSchema } from 'components/FieldCreate/schemas';

import Icon from '@ant-design/icons-react';


const dummyLocations = {
	type: 'FeatureCollection',
	features: [{
		type: 'Feature', properties: { strata: '0.0', id: '5e129fce252061571fe4f0cd', label: 0 }, geometry: { type: 'Point', coordinates: [-105.05867246794078, 40.14644050323539] }, id: '5e129fce252061571fe4f0cd',
	}, {
		type: 'Feature', properties: { strata: '0.0', id: '5e129fce252061571fe4f0ce', label: 1 }, geometry: { type: 'Point', coordinates: [-105.06156326616642, 40.146504810195616] }, id: '5e129fce252061571fe4f0ce',
	}, {
		type: 'Feature', properties: { strata: '1.0', id: '5e129fce252061571fe4f0cf', label: 2 }, geometry: { type: 'Point', coordinates: [-105.0638908834826, 40.14621349195033] }, id: '5e129fce252061571fe4f0cf',
	}, {
		type: 'Feature', properties: { strata: '2.0', id: '5e129fce252061571fe4f0d0', label: 3 }, geometry: { type: 'Point', coordinates: [-105.05954858586465, 40.14712656311278] }, id: '5e129fce252061571fe4f0d0',
	}, {
		type: 'Feature', properties: { strata: '2.0', id: '5e129fce252061571fe4f0d1', label: 4 }, geometry: { type: 'Point', coordinates: [-105.06962988657972, 40.146660000145616] }, id: '5e129fce252061571fe4f0d1',
	}, {
		type: 'Feature', properties: { strata: '2.0', id: '5e129fce252061571fe4f0d2', label: 5 }, geometry: { type: 'Point', coordinates: [-105.06946750062477, 40.148111163742996] }, id: '5e129fce252061571fe4f0d2',
	}, {
		type: 'Feature', properties: { strata: '2.0', id: '5e129fce252061571fe4f0d3', label: 6 }, geometry: { type: 'Point', coordinates: [-105.05982072136563, 40.1470601668423] }, id: '5e129fce252061571fe4f0d3',
	}, {
		type: 'Feature', properties: { strata: '3.0', id: '5e129fce252061571fe4f0d4', label: 7 }, geometry: { type: 'Point', coordinates: [-105.06550518307876, 40.14707593435057] }, id: '5e129fce252061571fe4f0d4',
	}, {
		type: 'Feature', properties: { strata: '3.0', id: '5e129fce252061571fe4f0d5', label: 8 }, geometry: { type: 'Point', coordinates: [-105.07091055788594, 40.14682604019358] }, id: '5e129fce252061571fe4f0d5',
	}, {
		type: 'Feature', properties: { strata: '3.0', id: '5e129fce252061571fe4f0d6', label: 9 }, geometry: { type: 'Point', coordinates: [-105.06733699187605, 40.14789920855807] }, id: '5e129fce252061571fe4f0d6',
	}, {
		type: 'Feature', properties: { strata: '3.0', id: '5e129fce252061571fe4f0d7', label: 10 }, geometry: { type: 'Point', coordinates: [-105.06514977687908, 40.14686465355478] }, id: '5e129fce252061571fe4f0d7',
	}, {
		type: 'Feature', properties: { strata: '3.0', id: '5e129fce252061571fe4f0d8', label: 11 }, geometry: { type: 'Point', coordinates: [-105.06730697211879, 40.14794948437129] }, id: '5e129fce252061571fe4f0d8',
	}, {
		type: 'Feature', properties: { strata: '3.0', id: '5e129fce252061571fe4f0d9', label: 12 }, geometry: { type: 'Point', coordinates: [-105.06119723786307, 40.14808188563738] }, id: '5e129fce252061571fe4f0d9',
	}, {
		type: 'Feature', properties: { strata: '3.0', id: '5e129fce252061571fe4f0da', label: 13 }, geometry: { type: 'Point', coordinates: [-105.06297931888076, 40.14767417119562] }, id: '5e129fce252061571fe4f0da',
	}, {
		type: 'Feature', properties: { strata: '3.0', id: '5e129fce252061571fe4f0db', label: 14 }, geometry: { type: 'Point', coordinates: [-105.07086360499834, 40.14706272272676] }, id: '5e129fce252061571fe4f0db',
	}, {
		type: 'Feature', properties: { strata: '5.0', id: '5e129fce252061571fe4f0dc', label: 15 }, geometry: { type: 'Point', coordinates: [-105.06725443391781, 40.14633054820623] }, id: '5e129fce252061571fe4f0dc',
	}],
};
// import { fieldFeatureCollection } from 'data/exampleField';
// console.log(JSON.stringify(fieldFeatureCollection));

export default function FieldLocations({ onBack, field, onNext }) {
	const [state, setState] = useState({
		locations: null,
		modalIsVisible: false,
	});
	// const [modalIsVisible, setModalIsVisible] = useState()
	function handleDownloadField() {
		download(
			JSON.stringify({ type: 'FeatureCollection', features: [field] }),
			'field_features.geojson',
			'application/geo+json',
		);
	}
	function handleCopyField() {
		copyToClipboard(
			JSON.stringify({ type: 'FeatureCollection', features: [field] }),
		);
	}

	function generateLocations() {
		const locations = randomPointsInPoly(field, 25);
		setState({ ...state, locations });
	}

	// function handleModalButton() {
	// 	setState({ ...state, modalIsVisible: true })
	// }

	const locationsIsValid = state.locations
		&& state.locations.features
		&& markersSchema.isValidSync(state.locations.features);

	console.log(field);
	const fieldIsValid = field && fieldFeatureSchema.isValidSync(field);

	return (
		<div className="field-locations">
			<header className="page-header text-align-center position-relative">

				<div className="position-absolute left-0 vertical-center-top-transform">
					{/*
					<button
						type="button"
						className="button circle icon no-bg position-absolute left-0 vertical-center-top-transform"
						onClick={onBack}
					>
						<Icon type="arrow-left-o" />
					</button> */}
					<button type="button" className="button  circle icon no-bg position-absolute left-0 vertical-center-top-transform" onClick={onBack}>
						{/* <button type="button" className="button circle icon-only" onClick={onBack}> */}
						{/* Back */}
						<Icon type="arrow-left-o" />
					</button>
				</div>
				<div>
					<h2 className="page-title">Field Create</h2>
					<div className="page-subtitle">Create Survey Locations</div>
				</div>
			</header>
			<MapboxGLMap
				// dragPan={false}
				features={{ type: 'FeatureCollection', features: [field] }}
				markers={state.locations || { type: 'FeatureCollection', features: [] }}
				// markers={state.locations || dummyLocations}
				// markers={state.locations}
				height="calc(100vh - 250px)"
				width="100%"
			/>
			{/* <pre>
				<textarea
					// style={{ display: 'block', width: '100%', height }}
					rows="10"
					cols="45"
					value={JSON.stringify(field)}
					readOnly
				/>
				{JSON.stringify(field)}
			</pre> */}
			<div className="padding-10 text-align-left">
				<div className="">
					{/* <div className="text-align-center">Survey Locations&nbsp;</div> */}
					<div className="text-align-center">
						{/* <div className="flex justify-content-space-between align-items-center padding-10"> */}

						<button
							type="button"
							className="button"
							onClick={generateLocations}
							disabled={!fieldIsValid}
						>
							Generate
						</button>
						{/* &nbsp;or */}
						{/* <span className="font-size-75">or</span> */}
						<div className="or font-size-75">OR</div>
						<button
							type="button"
							className="button"
							onClick={() => setState({ ...state, modalIsVisible: true })}
						>
							Import
						</button>
					</div>
				</div>
				<div className=" margin-top-25 text-align-center">
					<span>
Field Boundaries Export
						{/* &nbsp;<Icon type="gateway-o" /> */}
					</span>
&nbsp;&nbsp;&nbsp;&nbsp;
					{/* <br/> */}
					<button type="button" className="button circle icon-only" onClick={handleDownloadField}>
						{/* Download  */}
						{/* Field&nbsp; */}
						{/* GeoJSON */}
						<Icon type="download-o" />
						{/* <Icon type="vertical-align-bottom-o" /> */}
					</button>
					<button type="button" className="button circle icon-only" onClick={handleCopyField}>
						{/* Copy  */}
						{/* Field&nbsp; */}
						<Icon type="copy-o" />
						{/* GeoJSON */}
					</button>
				</div>

				{/* <div className="flex justify-content-space-between margin-top-10"> */}
				{(
					<div className="text-align-center padding-25">
						<button
							type="button"
							className="button primary"
							onClick={() => onNext(state.locations)}
							disabled={!locationsIsValid}
						>
							Create Field
						</button>
					</div>
				)}
			</div>
			<MarkersFileUploadModal
				isVisible={state.modalIsVisible}
				onClose={() => setState({ ...state, modalIsVisible: false })}
				onSubmit={(locations) => setState({ ...state, modalIsVisible: false, locations })}
			/>
		</div>
	);
}
