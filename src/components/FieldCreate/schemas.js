import * as yup from 'yup';

// const fieldCoordinateSchema = yup.array().min(2).max(2).of(yup.number());
// Allow third value in coordinates for ... altitude? or something?
const fieldCoordinateSchema = yup.array().min(2).max(3).of(yup.number());

const fieldGeometrySchema = yup.object().required().shape({
	type: yup.string().matches(/^Polygon$/),
	coordinates: yup.array().min(1).max(1)
		.of(yup.array().of(fieldCoordinateSchema)),
});

const fieldFeatureSchema = yup.object().shape({
	id: yup.string().required(),
	type: yup.string().matches(/^Feature$/).required(),
	properties: yup.object(),
	geometry: fieldGeometrySchema,
});

const fieldFeaturesSchema = yup.array().min(1).max(1).of(fieldFeatureSchema);

const markerGeometrySchema = yup.object().required().shape({
	type: yup.string().matches(/^Point$/),
	coordinates: fieldCoordinateSchema,
});

const markerFeatureSchema = yup.object().shape({
	// id: yup.mixed().oneOf([yup.string(), yup.number()]).required(),
	// id: yup.mixed().required(),
	type: yup.string().matches(/^Feature$/).required(),
	properties: yup.object(),
	geometry: markerGeometrySchema,
});

const markersSchema = yup.array().min(1).of(markerFeatureSchema);

export {
	fieldCoordinateSchema,
	fieldGeometrySchema,
	fieldFeatureSchema,
	fieldFeaturesSchema,
	markerFeatureSchema,
	markerGeometrySchema,
	markersSchema,
};
