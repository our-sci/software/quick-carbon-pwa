import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import MapboxGLMapSearchDraw from 'components/MBMap/MapboxGLMapSearchDraw';
import FileUploadModal from 'components/FieldCreate/FieldFileUploadModal';
// import OLMapDraw from 'components/OLMap/OLMapDraw';
// import * as yup from 'yup';
import { fieldFeaturesSchema } from './schemas';
import Icon from '@ant-design/icons-react';



export default function FieldDraw({ 
	// fieldState, 
	// setFieldState,
	field,
	setField,
	onNext,
}) {
	const [modalIsVisible, setModalIsVisible] = useState(false); 
	// const [drawState, setDrawState] = useState({ features: [field], fileModalIsVisible: false });
	const [drawState, setDrawState] = useState({ features: [field] });

	function drawCreateHandler(ev) {
		// console.log(ev);
		// setDrawState({ ...drawState, geometry: ev.features[0].geometry, id: ev.features[0].id });
		const { features } = ev.drawInstance.getAll();
		setDrawState({ ...drawState, features });
	}

	function drawUpdateHandler(ev) {
		// console.log(ev);
		// setDrawState({ ...drawState, geometry: ev.features[0].geometry, id: ev.features[0].id });
		const { features } = ev.drawInstance.getAll();
		setDrawState({ ...drawState, features });
	}

	function drawDeleteHandler(ev) {
		// console.log(ev);
		const { features } = ev.drawInstance.getAll();
		setDrawState({ ...drawState, features });
	}

	// function setDrawInstance(drawInstanceRef) {
	// 	setDrawState({ ...drawState, drawInstanceRef });
	// }

	function nextHandler() {
		console.log(drawState.features[0]);
		onNext(drawState.features[0]);
	}

	const fieldFeaturesIsValid = fieldFeaturesSchema.isValidSync(drawState.features);

	// function fileUploadSetField(feature) {
	// 	console.log(feature);
	// 	setDrawState({ ...drawState, field: feature });
	// }

	return (
		<div className="stage-1">
			<header className="page-header text-align-center position-relative">
				<Link to="/field" className="position-absolute left-10 vertical-center-top-transform">
					<button type="button" className="button icon-only circle no-bg">
						<Icon type="arrow-left-o" />
						{/* Back */}
					</button>
				</Link>
				<h1 className="page-title">Field Create</h1>
				<div className="page-subtitle">Draw Field Boundaries</div>
			</header>
			<MapboxGLMapSearchDraw
				drawCreateHandler={drawCreateHandler}
				drawUpdateHandler={drawUpdateHandler}
				drawDeleteHandler={drawDeleteHandler}
				features={{ type: 'FeatureCollection', features: [field] }}
				hasDrawControl={true}
				hasGeolocateControl={true}
				hasGeocoderControl={true}
				width="100%"
				height="calc(100vh - 125px)"
				// initialBounds={bbox(exampleField)}
			// setDrawInstance={setDrawInstance}
			/>
			{/* <OLMapDraw /> */}
			<div className="flex justify-content-space-between padding-10">
				
				<button
					type="button"
					// onClick={() => setDrawState({ ...drawState, fileModalIsVisible: true })}
					onClick={() => setModalIsVisible(true)}
					className="button"
				>
					Import
				</button>
				<button
					type="button"
					className="button primary"
					disabled={!fieldFeaturesIsValid}
					onClick={nextHandler}
				>
					Continue
				</button>

			</div>
			<FileUploadModal
				// isVisible={drawState.fileModalIsVisible} 
				isVisible={modalIsVisible} 
				// onClose={() => setDrawState({ ...drawState, fileModalIsVisible: false })}
				onClose={() => setModalIsVisible(false)}
				setField={setField}
			/>
		</div>
	);
}

