import {
	// fieldCoordinateSchema,
	// fieldGeometrySchema,
	// fieldFeatureSchema,
	fieldFeaturesSchema,
	markersSchema,
} from './schemas';
// import * as yup from 'yup';

const validFeatures = {
	features: [{
		id: 'eb826a9f515124e113263cc0fc56ca60',
		type: 'Feature',
		properties: {},
		geometry: {
			coordinates: [
				[
					[-121.723488671875, 39.284369474109354],
					[-121.921242578125, 37.615332191492556],
					[-119.43833242187503, 37.615332191492556],
					[-121.723488671875, 39.284369474109354],
				],
			],
			type: 'Polygon',
		},
	}],
};

const invalidFeatures = {
	features: [
		{
			id: 'eb826a9f515124e113263cc0fc56ca60',
			type: 'Feature',
			properties: {},
			geometry: {
				coordinates: [
					[
						[-121.723488671875, 39.284369474109354],
						[-121.921242578125, 37.615332191492556],
						[-119.43833242187503, 37.615332191492556],
						[-121.723488671875, 39.284369474109354],
					],
				],
				type: 'Polygon',
			},
		},
		{
			id: '9f518f671b633e23c54dce099fe9b701',
			type: 'Feature',
			properties: {},
			geometry: {
				coordinates: [
					[
						[-121.56968007812503, 40.83148833499612],
						[-122.58042226562486, 39.26736028845883],
						[-120.185402734375, 39.18225243322752],
						[-121.56968007812503, 40.83148833499612],
					],
				],
				type: 'Polygon',
			},
		},
	],
};

const validMarkers = {
	type: 'FeatureCollection',
	features: [
		{
			type: 'Feature',
			id: 0,
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-123.2353148577924, 41.60483923142181],
			},
		},
		{
			type: 'Feature',
			id: 1,
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-123.84414023288585, 41.74028696179343],
			},
		},
		{
			type: 'Feature',
			id: 2,
			properties: {},
			geometry: {
				type: 'Point',
				coordinates: [-122.67612012552448, 42.73191293228222],
			},
		},
	],
};

it('validates correctly', () => {
	expect(fieldFeaturesSchema.isValidSync(validFeatures.features)).toBeTruthy();
	expect(fieldFeaturesSchema.isValidSync(invalidFeatures.features)).toBeFalsy();
	expect(fieldFeaturesSchema.isValidSync([])).toBeFalsy();
});

it('validates markers correctly', () => {
	markersSchema.validateSync(validMarkers.features);
	expect(markersSchema.isValidSync(validMarkers.features)).toBeTruthy();
	expect(markersSchema.isValidSync(validFeatures.features)).toBeFalsy();
});
