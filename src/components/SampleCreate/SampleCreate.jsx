import React from 'react';
import {
	Formik,
	Form,
	Field,
	ErrorMessage,
} from 'formik';
import InputField from 'components/Form/InputField';
import RadioButton from 'components/Form/RadioButton';
import RadioButtonGroup from 'components/Form/RadioButtonGroup';
import Select from 'components/Form/Select';
import classNames from 'classnames';
import * as Yup from 'yup';
import 'components/Form/form.css';
import ObjectID from 'bson-objectid';
// import { soilDepths } from 'data/db';
// import { surveyId } from 'data/exampleSampleCreateProps';

const validationSchema = Yup.object().shape({
	surveyId: Yup.string().required(),
	sampleId: Yup.string().required('required'),
	depthId: Yup.string().required('required'),
	locationId: Yup.string().required(),
	timestamp: Yup.number(), // or string
});

export default function SampleCreate({
	location,	
	depths,
	surveyId,
	// position,
	depthId, // optional, if not present then ask for use input
	onSubmit,
	hiddenId, // optional
	sampleId, // optional
	timestamp,
	coordinates,
}) {
	function handleSubmit({ id, coordinates, ...rest }, { setSubmitting }) {
		// console.log(values);
		onSubmit({
			properties: {
				...rest,
			},
			// id: id || window.btoa(ObjectID().id),
			id,
			geometry: {
				type: 'Point',
				coordinates,
			},
		});
		setSubmitting(false);
	}

	const initialValues = {
		id: hiddenId || ObjectID().str,
		surveyId: surveyId,
		sampleId: sampleId || '',
		depthId: depthId || '',
		locationId: location.id,
		// timestamp: position.timestamp,
		timestamp: timestamp,
		// coordinates: [
		// 	position.coords.longitude, 
		// 	position.coords.latitude,
		// ],
		coordinates: coordinates,
	};

	const sortedDepths = depths.sort((a, b) => a.min_depth - b.min_depth);
	const depth = sortedDepths.filter((depth) => depth.handle === depthId)[0];
	console.log(depth);

	return (
		<div className="sample-create">
			<div className="margin-bottom-10 text-align-center strong">
				New Sample
			</div>
			{/* <div>
				{location.id}
			</div> */}
			<Formik 
				initialValues={initialValues}
				validationSchema={validationSchema}
				onSubmit={handleSubmit}
			>
				{({
					isSubmitting,
					// handleSubmit,
					values,
					errors,
					touched,
					setFieldValue,
					setFieldTouched,
				}) => (
					<Form className="">
						<Field
							component={InputField}
							type="text"
							name="sampleId"
							id="sampleId"
							label="Sample ID"
							autoFocus={true}
						/>
						{/* <RadioButtonGroup
							id="depthId"
							label="Depth"
							value={values.depthId}
							error={errors.depthId}
							touched={touched.depthId}
							disabled
						>
							{sortedDepths.filter((depth) => depth.handle === values.depthId)
								.map((depth) => (
									<Field
										component={RadioButton}
										name={'depthId'}
										id={depth.handle}
										label={depth.name}
										key={depth.handle}
										className="font-size-75"
										disabled
									/>
							))}
						</RadioButtonGroup> */}

						<Field
							component={Select}
							type="text"
							name="depthId"
							id="depthId"
							label="Depth"
							inputClassName="font-size-75"
							value={values.depthId}
							disabled
						>
							<option value={values.depthId}>
								{depth.name}
							</option>
						</Field>
						
						{/* <Field
							component={InputField}
							type="text"
							name="locationId"
							id="locationId"
							label="Location ID"
							disabled
						/> */}
						<Field
							component={Select}
							type="text"
							name="locationId"
							id="locationId"
							label="Location ID"
							inputClassName="font-size-75"
							disabled
						>
							<option value={values.locationId}>
								Location {location.properties.label}
							</option>
						</Field>
						
						<Field
							component={Select}
							type="text"
							name="surveyId"
							id="surveyId"
							label="Survey ID"
							inputClassName="font-size-75"
							disabled
						>
							<option value={values.surveyId}>
								{surveyId}
							</option>
						</Field>
						<Field
							component={InputField}
							type="text"
							name="coordinates"
							id="coordinates"
							label="Coordinates"
							disabled
						/>
						{/* <Field
							component={InputField}
							type="text"
							name="timestamp"
							id="timestamp"
							label="Timestamp"
							disabled
						/> */}
						<Field
							component={Select}
							type="text"
							name="timestamp"
							id="timestamp"
							label="Timestamp"
							inputClassName="font-size-75"
							disabled
						>
							<option value={values.timestamp}>
								{(new Date(values.timestamp)).toLocaleString()}
							</option>
						</Field>


						<div className="margin-top-25 text-align-center">
							<button type="submit" className="button primary " disabled={isSubmitting}>
								Add
							</button>
						</div>
					</Form>
				)}
			</Formik>
		</div>
	)
}

