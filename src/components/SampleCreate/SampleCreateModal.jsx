import React from 'react';
import Modal from 'components/Modal/Modal';
import SampleCreate from 'components/SampleCreate/SampleCreate';

export default function SampleCreateModal({
	isVisible,
	onClose,
	location,
	depths,
	surveyId,
	hiddenId,
	position,
	onSubmit,
	depthId,
	sampleId,
	timestamp,
	coordinates,
}) {
	// console.log('modal position', position)
	return (
		<Modal
			isVisible={isVisible} 
			onClose={onClose}
			height="auto"
			width="85%"
			verticalCenter={false}
		>
			<SampleCreate 
				location={location}
				surveyId={surveyId}
				depths={depths}
				position={position}
				onSubmit={onSubmit}
				depthId={depthId}
				sampleId={sampleId}
				hiddenId={hiddenId}
				timestamp={timestamp}
				coordinates={coordinates}
			/>
		</Modal>
	)
}