import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import Modal from 'components/Modal/Modal';
import { withRouter } from 'react-router-dom';
import AppContext from 'contexts/AppContext';
// import handleize from 'utils/handleize';
import classNames from 'classnames';
// import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { organizations } from 'data/db';
import styles from './SetupModal.module.css';


function Option({ id, name, disabled }, index) {
	return (
		// <option value={handleize(name)} disabled={disabled} selected={index === 0} key={index}>
		<option value={id} key={id}>
			{name}
		</option>
	);
}



export function SetupModal({ location }) {
	const [appState, setAppState] = useContext(AppContext);
	const [formState, setFormState] = useState({ organization: undefined });

	const selectClasses = classNames([
		'display-block',
		'select',
		'tall-select',
		styles['tall-select'],
		'font-weight-700'
	]);

	function formIsValid() {
		return formState.organization !== undefined && /.*\S.*/.test(formState.organization);
	}

	function handleOrganizationChange(event) {
		setFormState({ ...formState, organization: event.target.value });
	}

	function handleSubmit(ev) {
		ev.preventDefault();
		// console.log(ev.target);
		setAppState({ ...appState, organization: formState.organization });
	}

	const isVisible = !appState.organization && location.pathname !== '/';

	return (
		// <ReactCSSTransitionGroup 
		// 	transitionName="fade"
		// 	transitionEnterTimeout={1500}
		// 	transitionLeaveTimeout={1000}
		// 	transitionAppear={true}
		// 	transitionLeave={true}
		// >

		<Modal
			className={styles['setup-modal']}
			isVisible={isVisible}
			onClose={() => {}}
			options={{ showClose: false }}
		>
			<h3>Select an Organization</h3>
			<form onSubmit={handleSubmit}>
				<select
					size={organizations.length} 
					className={selectClasses} 
					onChange={handleOrganizationChange}
					value={formState.organization}
				>
					{organizations.map(Option)}
				</select>
				<button 
					className="display-block button primary"
					type="submit"
					disabled={!formIsValid()}
				>
					Continue
				</button>
			</form>

		</Modal>
		// </ReactCSSTransitionGroup>
	);
}

SetupModal.propTypes = {
	location: PropTypes.shape({
		key: PropTypes.string,
		pathname: PropTypes.string,
		search: PropTypes.string,
		hash: PropTypes.string,
		state: PropTypes.any,
	}).isRequired,
};

export default withRouter(SetupModal);
