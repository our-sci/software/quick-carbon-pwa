import React, { useState, useContext } from 'react';
import SurveyCreate from 'components/SurveyCreate/SurveyCreate3';
import { PositionContextProvider } from 'contexts/PositionContext';
import EventEmitter from 'events';

function Dummy({ cb }) {
	const [dummyState, setDummyState] = useState(null);
	return (
		<div>

		</div>
	)
}

export default function SurveyCreatePage({ match }) {
	// const eeRef = useRef();

	// eeRef.current = new EventEmitter();

	function geoCb(pos) {
		console.log(pos);
	}

	return (
		<div className="survey-create-page page">	
			{/* <PositionContextProvider> */}
				<SurveyCreate fieldId={match.params.id} geoCb={geoCb} />
			{/* </PositionContextProvider> */}
			
		</div>
	);
}
