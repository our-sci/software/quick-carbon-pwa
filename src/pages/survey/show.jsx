import React, { useContext, useState, useEffect } from 'react';
import AppContext from 'contexts/AppContext';
import MapboxGLMap from 'components/MBMap/MapboxGLMapSearchDraw';
import Icon from '@ant-design/icons-react';
import { Link } from 'react-router-dom';
import generateResults from 'utils/generateResults';
import { get } from 'http';

function getField(id, fields) {
	return fields.find((field) => String(field.id) === id);
}

function getFieldBySurvey(id, fields) {
	return fields.find((field) => field && field.properties && field.properties.samplingEvents
		&& field.properties.samplingEvents.some(
			(surveyId) => id === surveyId
		)
	);
}

function getSurvey(id, surveys) {
	return surveys.find((survey) => survey._id === id);
}

function getLocations(field, surveyLocations) {
	return surveyLocations.find(
		(locations) => String(field && field.properties && field.properties.surveyLocations) === String(locations.id),
	) || {};
}
function getLocation(id, locations) {
	if (!(id && locations && locations.features && locations.features.length > 0)) {
		return {};
	}
	return locations.features.find((l) => l.properties.id === id);
}

function getDepth(id, depths) {
	return depths.find(
		(depthObj) => id === depthObj.handle || id === depthObj.id,
	);
}

function getDepths(ids, depths) {
	return ids.map((depthId) => getDepth(depthId, depths))
		.sort((a, b) => a.min_depth - b.min_depth);
}

function featureCollection(features) {
	return {
		type: 'FeatureCollection',
		features,
	};
}


function getMarkers(locations) {
	if (!(locations && locations.features && locations.features.length > 0)) {
		return featureCollection([]);
	}
	const markersArray = locations.features.map((location) => ({
			...location,
			properties: {
				...location.properties,
				markerType: 'LOCATION_COLOR_INTERPOLATE',
				value: Math.random(),
			},
	}));
	return featureCollection(markersArray);
}

function getResultsForLocation(locationId, results) {
	// console.log('results', results, locationId);
	if (!(results && results.length > 0)) {
		return [];
	}
	return results.filter((result) => result.properties.locationId === locationId);
}

export default function SurveyShow({ match }) {
	const [appState, setAppState] = useContext(AppContext);
	const [selectedLocationId, setSelectedLocationId] = useState(null);
	const [state, setState] = useState({});
	const [markers, setMarkers] = useState(featureCollection({}));
	const [fakeResults, setFakeResults] = useState([]);

	const surveyId = match.params.id;
	const survey = getSurvey(surveyId, appState.samplingEvents);
	const field = getFieldBySurvey(surveyId, appState.fields);
	const dataIsLoaded = field && field.properties && field.properties.surveyLocations;
	const fieldId = field && field.id;
	const locations = getLocations(field, appState.surveyLocations);
	const selectedLocation = getLocation(selectedLocationId, locations);
	const depths = (dataIsLoaded && getDepths(field.properties.depths, appState.soilDepths)) || [];



	useEffect(() => {
		console.log('locations', locations);
		// console.log('markers', markers);
		console.log('useeffect', getMarkers(locations));
		setMarkers(getMarkers(locations));
		dataIsLoaded && setFakeResults(generateResults(locations, field.properties.depths, surveyId));
	}, [appState]);

	const activeResults = getResultsForLocation(selectedLocationId, fakeResults);
	console.log(activeResults);


	function handleMapClick(ev) {
		const features = ev.target.queryRenderedFeatures(ev.point);
		if (features && features[0]) {
			if (features[0].properties.id !== selectedLocationId) {
				setSelectedLocationId(features[0].properties.id);
			} else {
				setSelectedLocationId(null);
			}
		}

	}

	return (
		<div className="survey-show-page">
			<header className="page-header text-align-center position-relative">
				<Link to={`/field/${fieldId}`} className="position-absolute left-10 vertical-center-top-transform">
					<button type="button" className="button icon-only circle no-bg">
						<Icon type="arrow-left-o" />
					</button>
				</Link>
				<h1 className="page-title">Survey Show</h1>
				<div className="page-subtitle">&nbsp;</div>
			</header>
			<section>
				{dataIsLoaded && (
					<>
						<div className="top position-relative">
							<MapboxGLMap
								features={{ type: 'FeatureCollection', features: [field] }}
								markers={markers}
								height="calc(100vh - 300px)"
								width="100%"
								eventListeners={[
									{
										type: 'click',
										listener: handleMapClick,
										layerId: 'markers',
									},
								]}
							/>
						</div>
					</>
				)}
				{/* {selectedLocationId && activeResults && (
					<table style={{width: '100%'}}>
						<tbody>
							<tr>
								<th></th>
								{depths.map((depth) => (
									<th key={depth.handle}>
										{depth.name}
									</th>
								))}
							</tr>
							<tr>
								<td>
									{selectedLocation.properties.label}
								</td>
								{activeResults.map((result) => (
									<td key={result.id} className="text-align-center">
										{result.properties.percentCarbon.toFixed(2)}%
									</td>
								))}
							</tr>
						</tbody>
					</table>
				)}
				{!dataIsLoaded && (
					<div className="text-align-center">
						<Icon type="loading-o" className="anticon-spin" />
					</div>
				)} */}
				{activeResults && (
					<div style={{ height: '200px', overflow: 'auto'}}>

						<table style={{width: '100%',}}>
							<tbody>
								<tr>
									<th></th>
									{depths.map((depth) => (
										<th key={depth.handle}>
											{depth.name}
										</th>
									))}
								</tr>
								{console.log('locations', locations)}
								{locations && locations.features && locations.features.length > 0 
									&& locations.features.map((location) => (
										<tr key={location.id}>
											<td>
												{location.properties.label}
											</td>
											{ getResultsForLocation(location.id, fakeResults).map((result) => (
												<td key={result.id} className="text-align-center">
													{result.properties.percentCarbon.toFixed(2)}%
												</td>
											))}
											{/* {activeResults.map((result) => (
												<td key={result.id} className="text-align-center">
													{result.properties.percentCarbon.toFixed(2)}%
												</td>
											))} */}
										</tr>
								))}
							</tbody>
						</table>
					</div>
				)}
				{!dataIsLoaded && (
					<div className="text-align-center">
						<Icon type="loading-o" className="anticon-spin" />
					</div>
				)}
			</section>
		</div>
	)
}
