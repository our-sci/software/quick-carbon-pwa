import React, { useState, useContext, useEffect } from 'react';
import SurveyCreate from 'components/SurveyCreate/SurveyCreate';
import { PositionContextProvider } from 'contexts/PositionContext';
// import EventEmitter from 'events';
// import AppContext from 'contexts/AppContext';

const BACKEND_BASE_URL = process.env.REACT_APP_BACKEND_URL;


export default function SurveyCreatePage({ match }) {
	// const eeRef = useRef();

	// eeRef.current = new EventEmitter();

	// const [appState, setAppState] = useContext(AppContext);

	// useEffect(() => {
	// 	async function fetchData() {
	// 		let fetchedFields = [];
	// 		let fetchedLocationCollections = [];
	// 		try {
	// 			const fieldResponse = await window.fetch(`${BACKEND_BASE_URL}/field`);
	// 			fetchedFields = await fieldResponse.json();
	// 			console.log('fetched fields', fetchedFields);
	// 			// setAppState({ ...appState, fields: fieldBody });
	// 		} catch (err) {
	// 			console.log(err);
	// 		}

	// 		try {
	// 			const locationCollectionReponse = await window.fetch(`${BACKEND_BASE_URL}/locationcollection`);
	// 			fetchedLocationCollections = await locationCollectionReponse.json();
	// 			console.log('fetched location collection', fetchedLocationCollections);
	// 			// setAppState({ ...appState, fields: fieldBody });
	// 		} catch (err) {
	// 			console.log(err);
	// 		}

	// 		setAppState({
	// 			...appState,
	// 			fields: fetchedFields,
	// 			surveyLocations: fetchedLocationCollections,
	// 		});


	// 	}

	// 	fetchData();
	// }, []);

	function geoCb(pos) {
		console.log(pos);
	}

	return (
		<div className="survey-create-page page">	
			<PositionContextProvider>
				<SurveyCreate fieldId={match.params.id} geoCb={geoCb} />
			</PositionContextProvider>
			
		</div>
	);
}
