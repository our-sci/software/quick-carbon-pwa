import React, { useContext } from 'react';
import AppContext from 'contexts/AppContext';
import { Link } from 'react-router-dom';
import Icon from '@ant-design/icons-react';
import MapboxGLMap from 'components/MBMap/MapboxGLMapSearchDraw';


function getDepth(id, depths) {
	return depths.find(
		(depthObj) => id === depthObj.handle || id === depthObj.id,
	);
}

function getDepths(ids, depths) {
	return ids.map((depthId) => getDepth(depthId, depths))
		.sort((a, b) => a.min_depth - b.min_depth);
}

export default function FieldShowPage({ match }) {
	const [appState, setAppState] = useContext(AppContext);

	const field = appState.fields.find((field) => String(field.id) === match.params.id) || {};
	const dataIsLoaded = field && field.properties && field.properties.surveyLocations;
	const markers = appState.surveyLocations.find(
		(locations) => String(field && field.properties && field.properties.surveyLocations) === String(locations.id),
	) || {};
	const samplingEvents = field && field.properties && field.properties.samplingEvents
		&& appState.samplingEvents.filter(
			(samplingEvent) => field.properties.samplingEvents.some((id) => id === samplingEvent._id)
		);

	const depths = (dataIsLoaded && getDepths(field.properties.depths, appState.soilDepths)) || [];
	const depthsString = depths.map((depth) => depth.name).join(', ');


	// console.log(appState);
	// console.log(field);
	// console.log(field.properties.surveyLocations);
	// console.log(appState.surveyLocations);
	// console.log(markers);
	return (
		<div className="field-show-page page">
			<header className="page-header text-align-center position-relative">
				<Link to="/field" className="position-absolute left-10 vertical-center-top-transform">
					<button type="button" className="button icon-only circle no-bg">
						<Icon type="arrow-left-o" />
					</button>
				</Link>
				<h1 className="page-title">Field View</h1>
				{ field && field.properties && field.properties.name
					? (
						<div className="page-subtitle">{ field.properties.name }</div>
					) : (
						<div className="page-subtitle"></div>
					)
				}
			</header>
			{ field && field.properties && markers
				? (
					<>
						<MapboxGLMap
							features={{ type: 'FeatureCollection', features: [field] }}
							markers={markers}
							height="calc(100vh - 300px)"
							width="100%"
						/>
						<div className="text-align-center margin-top-10">
							<Link to={`/field/${field.id}/survey-create`} className="display-inline-block">
								<button type="button" className="button primary">
									Create New Survey
									&nbsp;<Icon type="plus-o" />
								</button>
							</Link>
							{/* <Link to={`/field/${field.id}/survey-create3`} className="display-inline-block">
									<Icon type="plus-o" />
							</Link> */}
						</div>
						{/* <table className="font-size-75 margin-l-r-auto margin-top-10">
							<tbody>
								{Object.entries(field.properties).map(([k, v]) => {
									return (
										<tr key={k}>
											<td>
												{k}
											</td>
											<td>
												{v}
											</td>
										</tr>
									)
								})}
							</tbody>
						</table> */}
						<div className="font-size-75 margin-left-10">
							<p>
								Depths: {depthsString}
							</p>
							<p>
								Email: {field.properties.email}
							</p>
						</div>
						<div className="font-size-75 margin-left-10">
							Surveys:
						</div>
						<ul className="font-size-75">
							{/* {JSON.stringify(samplingEvents)} */}
							{ samplingEvents.map((samplingEvent) => (
								<li key={samplingEvent._id}>
									<Link to={`/survey/${samplingEvent._id}`} >
										{(new Date(samplingEvent.completedAt)).toLocaleString()}
									</Link>
								</li>
							))}
						</ul>
					</>
				) : (
					<div className="text-align-center">
						<Icon type="loading-o" className="anticon-spin" />						
					</div>
				)	
			}	
		</div>
	);
}
