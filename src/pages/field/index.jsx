import React, { useContext, useEffect }from 'react';
// import OLMap from 'components/OLMap/OLMap';
import AppContext from 'contexts/AppContext';
import { Link } from 'react-router-dom';
import classNames from 'classnames';
// import twemoji from 'twemoji';
import styles from './index.module.css';
import Icon from '@ant-design/icons-react';


const BACKEND_BASE_URL = process.env.REACT_APP_BACKEND_URL;

// function FieldListItem({ name, samplingEvents }, index) {
function FieldListItem({ id, properties: { name } }, index) {
	return (
		<li key={index} className={styles['field-list-item']}>
			<span className={styles['field-list-item-icon']}>
				{/* 🚩 */}
				{/* {twemoji.parse('🚩')}
				{twemoji.parse('🗺️')}
			{twemoji.parse('🏞️')} */}
			</span>
			<Link to={`/field/${id}`} className="color-black link">
				<span className={styles['field-list-item-label']}>
					{name}
					&nbsp;<Icon type="info-circle-o" />
				</span>
			</Link>
			{/* <span className={styles['field-list-item-actions']}>
				<button type="button" className="button small no-bg icon">
					<span role="img">&#9999;</span>
				</button>
				<button type="button" className="button small no-bg icon">
					<span role="img">🗑</span>
				</button>
			</span> */}

		</li>
	);
}

export default function FieldIndexPage() {
	const [appState, setAppState] = useContext(AppContext);

	// useEffect(() => {
	// 	async function fetchData() {
	// 		let fetchedFields = [];
	// 		let fetchedLocationCollections = [];
	// 		try {
	// 			const fieldResponse = await window.fetch(`${BACKEND_BASE_URL}/field`);
	// 			fetchedFields = await fieldResponse.json();
	// 			console.log('fetched fields', fetchedFields);
	// 			// setAppState({ ...appState, fields: fieldBody });
	// 		} catch (err) {
	// 			console.log(err);
	// 		}

	// 		try {
	// 			const locationCollectionReponse = await window.fetch(`${BACKEND_BASE_URL}/locationcollection`);
	// 			fetchedLocationCollections = await locationCollectionReponse.json();
	// 			console.log('fetched location collection', fetchedLocationCollections);
	// 			// setAppState({ ...appState, fields: fieldBody });
	// 		} catch (err) {
	// 			console.log(err);
	// 		}

	// 		setAppState({ 
	// 			...appState, 
	// 			fields: fetchedFields,
	// 			surveyLocations: fetchedLocationCollections,
	// 		});


	// 	}

	// 	fetchData();
	// }, []);

	const pageClasses = classNames([styles['field-index-page'], 'page', 'text-align-center']);

	return (
		<div className={pageClasses}>
			<h2>Fields</h2>
			<p>
				{/* Some information about fields */}
			</p>
			{/* <OLMap /> */}
			<ul className={styles['field-list']}>
				{ appState.fields.map(FieldListItem)}
			</ul>
			<Link to="/field-create">
				<button type="button" className="button primary">
					+ New Field
				</button>
			</Link>

		</div>
	);
}
