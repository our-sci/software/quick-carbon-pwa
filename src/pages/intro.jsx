import React from 'react';
import { Link } from 'react-router-dom';
// import styles from './intro.module.css';

export default function introPage() {
	return (
		<div className="page intro-page text-align-center flex-center">
			<div className="center-wrapper">

				<h2>Welcome to Quick Carbon</h2>
				<div className="padding-25">
					<p className="text-align-left">
						{/* This is a paragraph to get you stoked about the project! */}
					</p>
					<p className="text-align-left">
						{/* And another to explain the process in more detail. */}
					</p>
				</div>
				<Link to="/field/">
					<button className="button primary">
						Get Started
					</button>
				</Link>
			</div>
		</div>
	);
}
