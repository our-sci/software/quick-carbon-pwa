import React, {
	createContext,
	useState,
	useMemo,
} from 'react';

const PositionContext = createContext();

export default PositionContext;

const initialState = null;

export function PositionContextProvider({ children }) {
	const [state, setState] = useState(initialState);
	const contextValue = useMemo(() => ([state, setState]), [state, setState]);
	return (
		<PositionContext.Provider value={contextValue}>
			{children}
		</PositionContext.Provider>
	);
}
