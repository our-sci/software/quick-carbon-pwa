import React, {
	createContext,
	useState,
	useMemo,
} from 'react';

import {
	fields,
	samplingEvents,
	samples,
	soilDepths,
	surveyLocations,
} from 'data/db';

const AppContext = createContext();

export default AppContext;

const initialState = {
	organization: 1,
	fields,
	samplingEvents,
	samples,
	soilDepths,
	surveyLocations,
};

export function AppContextProvider({ children }) {
	const [state, setState] = useState(initialState);
	const contextValue = useMemo(() => ([state, setState]), [state, setState]);
	return (
		<AppContext.Provider value={contextValue}>
			{children}
		</AppContext.Provider>
	);
}
