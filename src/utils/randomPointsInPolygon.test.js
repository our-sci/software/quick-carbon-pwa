import randomPointsInPolygon from './randomPointsInPolygon';
import exampleField from 'data/exampleField';
import expectExport from 'expect';

// it('generates points', () => {
// 	// const points = randomPointsInPolygon(exampleField, 25);
// 	// console.log(points)
// 	// expect(points).toEqual(expect.anything());
// }

describe('randomPointsInPolygon', () => {
	test('does something', () => {
		// expect(can1).toEqual(can2);
		const points = randomPointsInPolygon(exampleField, 25);
		// console.log(JSON.stringify(points))
		expect(points).toEqual(expect.anything());
	});
	// test('are not the exact same can', () => {
	// 	expect(can1).not.toBe(can2);
	// });
});
