/**
 * Convert a string to lowercase kebab-case for use as handle or ID
 * @param {string} str - string to handleize
 * @return {string} the transformed string
 */
export default function handleize(str) {
	return str && str.toLowerCase().replace(/\s/gi, '-');
}
