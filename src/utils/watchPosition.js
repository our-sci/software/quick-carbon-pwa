import { distance, nearestPoint, point } from '@turf/turf';

function handleSuccess(pos) {
	console.log(pos);
}

function handleError(err) {
	console.warn(err);
}

const options = {
	enableHighAccuracy: true,
	timeout: 6000,
	maximumAge: 0,
};


export default function watchPosition() {
	const id = window.navigator.geolocation.watchPosition(handleSuccess, handleError, options);
	return id;
}


/**
 * onSuccess
 * @param {*} points - GeoJSON FeatureCollection of points to be compared to
 * @param {function} successCb - Callback to execute once watchPosition executes and nearest point is calculated
 */
function onSuccess(points, successCb) {
	/**
	 * @callback successCb
	 * @param {*}  
	 * @param {*}  
	 */
	return (pos) => {
		console.log(pos);
		const { longitude, latitude } = pos.coords;
		const currentLocation = point([longitude, latitude]);
		const nearPoint = nearestPoint(currentLocation, points);
		const distanceToPoint = distance(currentLocation, nearPoint);
		successCb(nearPoint, distanceToPoint, currentLocation, pos);
	};
}

export function watchNearestLocation(points, successCb) {
	const id = window.navigator.geolocation.watchPosition(
		onSuccess(points, successCb),
		handleError,
		options,
	);
	return id;
}

