export default function copyToClipboard(str) {
	const el = document.createElement('textarea');
	el.value = str;
	el.style = {
		position: 'fixed',
		top: 0,
		left: 0,
		width: '1px',
		height: '1px',
		padding: 0,
		border: 'none',
		outline: 'none',
		boxShadow: 'none',
		background: 'transparent',
	};
	document.body.appendChild(el);
	el.select();
	document.execCommand('copy');
	// TODO: add error handling?
	// try {
	// 	var status = document.execCommand('copy');
	// 	if (!status) {
	// 		console.error("Cannot copy text");
	// 	} else {
	// 		console.log("The text is now on the clipboard");
	// 	}
	// } catch (err) {
	// 	console.log('Unable to copy.');
	// }
	el.remove();
}