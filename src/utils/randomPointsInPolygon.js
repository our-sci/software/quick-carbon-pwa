import {
	bbox,
	pointsWithinPolygon,
	// booleanPointInPolygon,
} from '@turf/turf';
import { randomPoint } from '@turf/random';
import ObjectID from 'bson-objectid';


// TODO: add density option, check polygon area
export default function randomPointsInPolygon(polygon, count, type = 'SURVEY_LOCATION_MARKER') {
	const polygonBbox = bbox(polygon);
	const pointsInPolygon = [];
	let remainder = Number(String(count));
	while (pointsInPolygon.length < count) {
		const points = randomPoint(remainder, { bbox: polygonBbox });
		const ptsInPoly = pointsWithinPolygon(points, polygon);
		pointsInPolygon.push(...ptsInPoly.features.slice(0, remainder));
		remainder -= ptsInPoly.features.length;
	}

	return {
		type: 'FeatureCollection',
		// features: pointsInPolygon.map((feature, i) => ({ ...feature, properties: { id: i }})),
		// features: pointsInPolygon.map((feature, i) => ({ ...feature, id: i, properties: { id: i} })),
		features: pointsInPolygon.map((feature, i) => {
			const id = ObjectID().str;
			return {
				...feature,
				id,
				properties: {
					id,
					label: String(i),
					type,
				},
			};
		}),
	};
}
