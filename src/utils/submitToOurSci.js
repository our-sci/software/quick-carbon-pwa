import { samples } from "data/db";

const SUBMISSION_URL = 'https://app.our-sci.net/api/survey/result/create';

const exampleMetadata = {
	instanceID: '',
	modified: '',
	created: '',
	uploaded: false,
	userID: '',
	organizationId: '',
};

const exampleFieldData = {
	field_id: '',
	field_name: '',
	oursci_share_email: '',
	oursci_share_dashboard: '',
	depths: `['0_TO_10_CM', '10_TO_20_CM']`,
	hand_texture: false,
	area: 0,
};

const defaultFieldData = {
		field_id: '',
		field_name: '',
		oursci_share_email: '',
		oursci_share_dashboard: '',
		depths: '[]',
		hand_texture: false,
		area: 0,
}

// const surveyKeysMap = {
// 	field_id: 'id',
// 	field_name: 'properties.name',
// 	oursci_share_email: 'properties.email',
// 	depths: '[]',
// 	hand_texture: 'properties.hasAdditionalQuestions',
// }
function mapFieldToBackendKeys({ field }) {
	// TODO validate field object with yup
	return {
		field_id: field.id,
		field_name: field.properties.name,
		oursci_share_email: field.properties.email,
		depths: JSON.stringify(field.properties.depths),
		hand_texture: field.properties.hasAdditionalQuestions,
		// area: 
	};
}

function mapSurveyToBackendKeys({ field, survey }) {
	return {
		survey_id: survey.id,
		field_id: field.id,
		oursci_share_email: field.properties.email,
		// oursci_share_dashboard:
		depths: JSON.stringify(field.properties.depths),
		hand_texture: field.properties.hasAdditionalQuestions,
		// area: 
	};
}

function mapSampleToBackendKeys({ sample, survey }) {
	return {
		pin_id: sample.id,
		sample_id: sample.properties.sampleId,
		survey_id: survey.id,
		depth: sample.properties.depthId,
		location: JSON.stringify(sample.geometry.coordinates),
		// oursci_share_email: 
		// oursci_share_dashboard: 
	};
}


function getDataTags(data) {
	return Object.entries(data)
		.map(([k, v]) => `<${k}>${v}</${k}>`)
		.join('');
}

function getDataTagsAndMapKeys(data, type) {
	// let remapper = null;
	let remappedData = {};
	switch (type) {
		case 'field':
			// remapper = mapFieldToBackendKeys;
			remappedData = mapFieldToBackendKeys({ field: data.field }, type);
			break;
		case 'survey':
			// remapper = 
			remappedData = mapSurveyToBackendKeys({ 
				survey: data.survey,
				field: data.field,
			});
			break;
		case 'sample':
			remappedData = mapSampleToBackendKeys({
				sample: data.sample,
				survey: data.survey,
			});
			break;
		default:
			// return;
			break;
	}
	// debugger;
	// return getDataTags(remapper(data));
	return getDataTags(remappedData);
}

function createXml(data, metadata, type) {
	// <instanceID>uuid:175c7cd7-ad9f-4f57-b0ba-875f6f6a871c</instanceID>
	// <instanceID>uuid:${uuidv4()}</instanceID>
	// <created>2019-11-06T08:21:55.035+01:00</created>
	// <modified>2019-11-06T08:22:36.445+01:00</modified>
	const meta = `
		<meta>
			<instanceID>${metadata.id}</instanceID>
			<modified>${metadata.createdAt}</modified>
			<created>${metadata.createdAt}</created>
			<uploaded>true</uploaded>
			<userID>unknown</userID>
			<organizationId>Quick Carbon</organizationId>
		</meta>
	`;

	let dataId = null;
	switch (type) {
		case 'field':
			dataId = 'build_QC-Fields_1573024897';
			break;
		case 'survey':
			dataId = 'build_QC-Surveys_1573096880';
			break;
			case 'sample':
				dataId = 'build_QC-Sample_1573180064';
				break;
		default:
			break;
	}

	const template = `<?xml version='1.0' ?><data id="${dataId}" xmlns:h="http://www.w3.org/1999/xhtml" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:jr="http://openrosa.org/javarosa">${meta}${getDataTagsAndMapKeys(data, type)}</data>`;

	return template;
}

export default async function submitSurvey(data, metadata, type) {
	const formData = new FormData();

	if (type === 'field' || type === 'sample' || type === 'survey') {

		const xmlString = createXml(data, metadata, type);
		const xmlBlob = new Blob([xmlString], { type: 'text/xml' });
		formData.append('files', new File(
			[xmlString],
			'data.xml', {
				type: 'text/xml',
			},
		));
		formData.append('override', false);
	}

	console.log('formdata', formData);
	const response = await fetch(SUBMISSION_URL, {
		method: 'POST',
		body: formData,
	});

	return await response.json();
}

	// const template0 = `
	// 	<?xml version='1.0' ?>
	// 	<data xmlns:h="http://www.w3.org/1999/xhtml" xmlns:jr="http://openrosa.org/javarosa"
	// 		xmlns:xsd="http://www.w3.org/2001/XMLSchema" id="build_QC-Fields_1573024897">
	// 		<meta>
	// 			<instanceID>uuid:175c7cd7-ad9f-4f57-b0ba-875f6f6a871a</instanceID>
	// 			<modified>2019-11-06T08:22:36.445+01:00</modified>
	// 			<created>2019-11-06T08:21:55.035+01:00</created>
	// 			<uploaded>true</uploaded>
	// 			<userID>JgSCLqbgLmhO3Le4aFWjuErryf53</userID>
	// 			<organizationId>Quick Carbon</organizationId>
	// 		</meta>
	// 		<field_id>${field.id}</field_id>
	// 		<field_name>rtujch</field_name>
	// 		<oursci_share_email>someemail</oursci_share_email>
	// 		<oursci_share_dashboard></oursci_share_dashboard>
	// 		<depths>5</depths>
	// 		<hand_texture>1</hand_texture>
	// 		<area>789</area>
	// 	</data>
	// `;