/**
 * Download a file
 * @param {String} data - file contents to be downloaded
 * @param {String} fileName - name of file to be downloaded
 * @param {String} contentType - content type of file-
 */
export default function download(data, fileName = 'field.geojson', contentType = 'application/geo+json') {
	// application/geo+json
	const el = document.createElement('a');
	const file = new Blob([data], {
		type: contentType,
	});
	el.href = URL.createObjectURL(file);
	el.download = fileName;
	el.click();
	URL.revokeObjectURL(el.href);
}