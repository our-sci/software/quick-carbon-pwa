import ObjectID from 'bson-objectid';

export default function generateResults(locations, depths, surveyId) {
	if (!(locations && locations.features)) {
		return [];
	}

	return locations.features.reduce((r, location, i) => {
		const features = depths.map((depthId) => {
			return {
				type: 'Feature',
				id: ObjectID().str,
				properties: {
					surveyId: surveyId,
					locationId: location.id,
					sampleId: `${Math.random()}`,
					timestamp: Date.now(),
					depthId: depthId,
					percentCarbon: Math.random() * 3,
				},
				geometry: location.geometry,

			};
		});
		const whatever = a => console.log('something')
		return [
			...r,
			...features,
			
		]
	}, []);
}
