export default function copyPosition(position) {
	const coordsKeys = [
		'accuracy',
		'altitude',
		'altitudeAccuracy',
		'heading',
		'latitude',
		'longitude',
		'speed',
	];
	const coordsCopy = coordsKeys.reduce((r, x, i) => {
		return {
			...r,
			[x]: position.coords[x],
		};
	}, {});

	return {
		timestamp: position.timestamp,
		coords: coordsCopy,
	};
}
