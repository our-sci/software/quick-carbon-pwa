import { distance, nearestPoint, point } from '@turf/turf';

export default function getNearestLocation(pos, points) {
	const { longitude, latitude } = pos.coords;
	const currentLocation = point([longitude, latitude]);
	return nearestPoint(currentLocation, points);
	// const distanceToPoint = distance(currentLocation, nearestLocation);
	// return { 
	// 	nearestLocation: nearestLocation, 
	// 	distance: distanceToPoint,
	// };
}
