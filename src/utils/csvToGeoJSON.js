import { csv2geojson } from 'csv2geojson';

// export default function csvToGeoJSON(csvString, { 
// 	rowDelimiter = '\n',
// 	colDelimiter = ',',
// }) {
// 	csvString.split(rowDelimiter);
// }

export default function csvToGeoJSON(csvString, options = {}) {
	return new Promise((resolve, reject) => {
		csv2geojson(csvString, options, (err, data) => {
			if (err) {
				reject(err);
			} else {
				resolve(data);
			}
		});
	});
}
