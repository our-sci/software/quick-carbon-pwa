module.exports = {
	parser: 'babel-eslint',
	extends: ['airbnb', 'airbnb/hooks'],
	rules: {
		indent: [2, 'tab', {
			SwitchCase: 1,
			VariableDeclarator: 1,
		}],
		'no-tabs': 0,
		'react/jsx-indent': [2, 'tab'],
		'react/jsx-indent-props': [2, 'tab'],
		
	},
	settings: {
		'import/resolver': {
			node: {
				paths: ['src'],
			},
		},
	},
	globals: {
		window: true,
		document: true,
	},
};
